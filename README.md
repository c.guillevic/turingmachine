****Projet de Programmation Fonctionnelle****

***Implémenter une Machine de Turing en OCaml***

L'objectif du projet est d'implémenter un modèle de calcul en OCaml.
Nous avons choisi la Machine de Turing comme modèle.

Si possible, nous allons réaliser une interface graphique en utilisant OCaml graphics (un module).

***Convention de nommage***

Nous utilisons la snake_case comme convention de nommage.