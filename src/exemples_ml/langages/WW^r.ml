let transitions_WW_r = [ 
Transition(Q(0) , Lettre('a') ,Q(10) , Epsilon , Right) ;
Transition(Q(0) , Lettre('b') ,Q(20) , Epsilon , Right) ;
Transition(Q(0) , Epsilon ,Q(90) , Epsilon , None) ;
Transition(Q(10) , Epsilon ,Q(11) , Epsilon , Left) ;
Transition(Q(10) , Wildcard ,Q(10) , Wildcard , Right) ;
Transition(Q(20) , Epsilon ,Q(21) , Epsilon , Left) ;
Transition(Q(20) , Wildcard ,Q(20) , Wildcard , Right) ;
Transition(Q(11) , Lettre('a') ,Q(30) , Epsilon , Left) ;
Transition(Q(21) , Lettre('b') ,Q(30) , Epsilon , Left) ;
Transition(Q(30) , Epsilon ,Q(0) , Epsilon , Right) ;
Transition(Q(30) , Wildcard ,Q(30) , Wildcard , Left)
];;
