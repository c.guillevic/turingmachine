let transitions_anbm_n_m = [ 
Transition(Q(0) , Lettre('a') ,Q(1) , Lettre('a') , Right) ;
Transition(Q(0) , Epsilon ,Q(8) , Epsilon , Left) ;
Transition(Q(0) , Wildcard ,Q(0) , Wildcard , Right) ;
Transition(Q(1) , Lettre('a') ,Q(1) , Lettre('a') , Right) ;
Transition(Q(1) , Lettre('b') ,Q(1) , Lettre('b') , Right) ;
Transition(Q(1) , Epsilon ,Q(2) , Epsilon , Left) ;
Transition(Q(1) , Lettre('B') ,Q(2) , Lettre('B') , Left) ;
Transition(Q(2) , Lettre('B') ,Q(2) , Lettre('B') , Left) ;
Transition(Q(2) , Lettre('b') ,Q(3) , Lettre('B') , Left) ;
Transition(Q(3) , Lettre('b') ,Q(3) , Lettre('b') , Left) ;
Transition(Q(3) , Lettre('a') ,Q(3) , Lettre('a') , Left) ;
Transition(Q(3) , Epsilon ,Q(4) , Epsilon , Right) ;
Transition(Q(3) , Lettre('A') ,Q(4) , Lettre('A') , Right) ;
Transition(Q(4) , Lettre('a') ,Q(1) , Lettre('A') , Right) ;
Transition(Q(4) , Lettre('b') ,Q(5) , Lettre('B') , Right) ;
Transition(Q(4) , Lettre('B') ,Q(5) , Lettre('B') , Right) ;
Transition(Q(5) , Lettre('b') ,Q(5) , Lettre('B') , Right) ;
Transition(Q(5) , Lettre('B') ,Q(6) , Lettre('B') , Right) ;
Transition(Q(6) , Lettre('B') ,Q(6) , Lettre('B') , Right) ;
Transition(Q(6) , Epsilon ,Q(7) , Epsilon , Left) ;
Transition(Q(7) , Lettre('A') ,Q(7) , Epsilon , Left) ;
Transition(Q(7) , Lettre('B') ,Q(7) , Epsilon , Left) ;
Transition(Q(7) , Epsilon ,Q(9) , Epsilon , None) ;
Transition(Q(8) , Epsilon ,Q(9) , Epsilon , None) ;
Transition(Q(8) , Wildcard ,Q(8) , Epsilon , Left)
];;
