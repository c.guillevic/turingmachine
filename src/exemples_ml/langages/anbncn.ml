let transitions_anbncn = [ 
Transition(Q(1) , Lettre('a') ,Q(2) , Lettre('A') , Right) ;
Transition(Q(1) , Lettre('B') ,Q(5) , Lettre('B') , Right) ;
Transition(Q(1) , Epsilon ,Q(7) , Epsilon , None) ;
Transition(Q(2) , Lettre('a') ,Q(2) , Lettre('a') , Right) ;
Transition(Q(2) , Lettre('B') ,Q(2) , Lettre('B') , Right) ;
Transition(Q(2) , Lettre('b') ,Q(3) , Lettre('B') , Right) ;
Transition(Q(3) , Lettre('b') ,Q(3) , Lettre('b') , Right) ;
Transition(Q(3) , Lettre('C') ,Q(3) , Lettre('C') , Right) ;
Transition(Q(3) , Lettre('c') ,Q(4) , Lettre('C') , Left) ;
Transition(Q(4) , Lettre('a') ,Q(4) , Lettre('a') , Left) ;
Transition(Q(4) , Lettre('b') ,Q(4) , Lettre('b') , Left) ;
Transition(Q(4) , Lettre('B') ,Q(4) , Lettre('B') , Left) ;
Transition(Q(4) , Lettre('C') ,Q(4) , Lettre('C') , Left) ;
Transition(Q(4) , Lettre('A') ,Q(1) , Lettre('A') , Right) ;
Transition(Q(5) , Lettre('B') ,Q(5) , Lettre('B') , Right) ;
Transition(Q(5) , Lettre('C') ,Q(5) , Lettre('C') , Right) ;
Transition(Q(5) , Epsilon ,Q(6) , Epsilon , Left) ;
Transition(Q(6) , Lettre('A') ,Q(6) , Epsilon , Left) ;
Transition(Q(6) , Lettre('B') ,Q(6) , Epsilon , Left) ;
Transition(Q(6) , Lettre('C') ,Q(6) , Epsilon , Left) ;
Transition(Q(6) , Epsilon ,Q(7) , Epsilon , None)
];;
