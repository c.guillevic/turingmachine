let transitions_double = [ 
Transition(Q(0) , Epsilon ,Q(9999) , Epsilon , None) ;
Transition(Q(0) , Wildcard ,Q(1) , Wildcard , None) ;
Transition(Q(1) , Lettre('|') ,Q(2) , Lettre('/') , Left) ;
Transition(Q(1) , Epsilon ,Q(9) , Epsilon , Left) ;
Transition(Q(1) , Wildcard ,Q(1) , Wildcard , Right) ;
Transition(Q(2) , Epsilon ,Q(1) , Lettre('/') , Right) ;
Transition(Q(2) , Wildcard ,Q(2) , Wildcard , Left) ;
Transition(Q(9) , Lettre('/') ,Q(9) , Lettre('|') , Left) ;
Transition(Q(9) , Epsilon ,Q(9999) , Epsilon , Right)
];;
