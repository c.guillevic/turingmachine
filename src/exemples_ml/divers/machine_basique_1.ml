let transitions_machine_basique_1 = [ 
Transition(Q(0) , Wildcard ,Q(1) , Lettre('M') , Right) ;
Transition(Q(11) , Wildcard ,Q(12) , Lettre('T') , Right) ;
Transition(Q(6) , Wildcard ,Q(7) , Lettre('e') , Right) ;
Transition(Q(17) , Lettre('d') ,Q(18) , Lettre('d') , None) ;
Transition(Q(5) , Wildcard ,Q(6) , Lettre('n') , Right) ;
Transition(Q(7) , Wildcard ,Q(8) , Epsilon , Right) ;
Transition(Q(8) , Wildcard ,Q(9) , Lettre('d') , Right) ;
Transition(Q(3) , Wildcard ,Q(4) , Lettre('h') , Right) ;
Transition(Q(16) , Wildcard ,Q(17) , Lettre('g') , Left) ;
Transition(Q(12) , Wildcard ,Q(13) , Lettre('u') , Right) ;
Transition(Q(4) , Wildcard ,Q(5) , Lettre('i') , Right) ;
Transition(Q(17) , Wildcard ,Q(17) , Wildcard , Left) ;
Transition(Q(14) , Wildcard ,Q(15) , Lettre('i') , Right) ;
Transition(Q(2) , Wildcard ,Q(3) , Lettre('c') , Right) ;
Transition(Q(9) , Wildcard ,Q(10) , Lettre('e') , Right) ;
Transition(Q(10) , Wildcard ,Q(11) , Epsilon , Right) ;
Transition(Q(1) , Wildcard ,Q(2) , Lettre('a') , Right) ;
Transition(Q(13) , Wildcard ,Q(14) , Lettre('r') , Right) ;
Transition(Q(15) , Wildcard ,Q(16) , Lettre('n') , Right)
];;
