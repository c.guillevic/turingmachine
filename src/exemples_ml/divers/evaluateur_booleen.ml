let transitions_evaluateur_booleen = [ 
Transition(Q(0) , Wildcard ,Q(1) , Wildcard , Left) ;
Transition(Q(1) , Epsilon ,Q(2) , Lettre('A') , Right) ;
Transition(Q(2) , Epsilon ,Q(3) , Lettre('A') , Left) ;
Transition(Q(2) , Wildcard ,Q(2) , Wildcard , Right) ;
Transition(Q(3) , Lettre('A') ,Q(10) , Lettre('A') , Right) ;
Transition(Q(3) , Wildcard ,Q(3) , Wildcard , Left) ;
Transition(Q(10) , Lettre('(') ,Q(20) , Lettre('[') , Right) ;
Transition(Q(10) , Lettre('[') ,Q(20) , Lettre('[') , Right) ;
Transition(Q(10) , Lettre('1') ,Q(30) , Wildcard , Right) ;
Transition(Q(10) , Lettre('0') ,Q(30) , Wildcard , Right) ;
Transition(Q(10) , Lettre('n') ,Q(50) , Wildcard , Right) ;
Transition(Q(10) , Lettre('A') ,Q(90) , Epsilon , Left) ;
Transition(Q(10) , Wildcard ,Q(10) , Wildcard , Right) ;
Transition(Q(20) , Lettre('(') ,Q(20) , Lettre('[') , Right) ;
Transition(Q(20) , Lettre(')') ,Q(23) , Lettre(']') , Left) ;
Transition(Q(20) , Lettre(']') ,Q(23) , Lettre(']') , Left) ;
Transition(Q(20) , Wildcard ,Q(20) , Wildcard , Right) ;
Transition(Q(23) , Lettre('[') ,Q(24) , Lettre('[') , Right) ;
Transition(Q(23) , Wildcard ,Q(23) , Wildcard , Left) ;
Transition(Q(24) , Lettre('|') ,Q(30) , Lettre('|') , None) ;
Transition(Q(24) , Lettre('&') ,Q(30) , Lettre('&') , None) ;
Transition(Q(24) , Lettre('n') ,Q(50) , Lettre('n') , Right) ;
Transition(Q(24) , Lettre(']') ,Q(25) , Epsilon , Left) ;
Transition(Q(24) , Wildcard ,Q(24) , Wildcard , Right) ;
Transition(Q(25) , Lettre('[') ,Q(40) , Epsilon , Left) ;
Transition(Q(25) , Wildcard ,Q(25) , Wildcard , Left) ;
Transition(Q(30) , Lettre('&') ,Q(31) , Lettre('&') , Right) ;
Transition(Q(30) , Lettre('|') ,Q(31) , Lettre('|') , Right) ;
Transition(Q(30) , Lettre('A') ,Q(90) , Epsilon , Left) ;
Transition(Q(30) , Wildcard ,Q(30) , Wildcard , Right) ;
Transition(Q(31) , Epsilon ,Q(31) , Epsilon , Right) ;
Transition(Q(31) , Lettre('(') ,Q(10) , Lettre('(') , None) ;
Transition(Q(31) , Lettre('[') ,Q(10) , Lettre('[') , None) ;
Transition(Q(31) , Lettre('0') ,Q(32) , Epsilon , Left) ;
Transition(Q(31) , Lettre('1') ,Q(34) , Epsilon , Left) ;
Transition(Q(31) , Lettre('n') ,Q(50) , Lettre('n') , Right) ;
Transition(Q(32) , Lettre('|') ,Q(40) , Epsilon , Left) ;
Transition(Q(32) , Lettre('&') ,Q(33) , Epsilon , Left) ;
Transition(Q(32) , Wildcard ,Q(32) , Wildcard , Left) ;
Transition(Q(33) , Lettre('1') ,Q(40) , Lettre('0') , Left) ;
Transition(Q(33) , Lettre('0') ,Q(40) , Lettre('0') , Left) ;
Transition(Q(33) , Wildcard ,Q(33) , Wildcard , Left) ;
Transition(Q(34) , Lettre('&') ,Q(40) , Epsilon , Left) ;
Transition(Q(34) , Lettre('|') ,Q(35) , Epsilon , Left) ;
Transition(Q(34) , Wildcard ,Q(34) , Wildcard , Left) ;
Transition(Q(35) , Lettre('1') ,Q(40) , Lettre('1') , Left) ;
Transition(Q(35) , Lettre('0') ,Q(40) , Lettre('1') , Left) ;
Transition(Q(35) , Wildcard ,Q(35) , Wildcard , Left) ;
Transition(Q(40) , Lettre('A') ,Q(10) , Lettre('A') , Right) ;
Transition(Q(40) , Wildcard ,Q(40) , Wildcard , Left) ;
Transition(Q(50) , Lettre('(') ,Q(10) , Lettre('(') , None) ;
Transition(Q(50) , Lettre('[') ,Q(10) , Lettre('[') , None) ;
Transition(Q(50) , Lettre('1') ,Q(51) , Lettre('0') , Left) ;
Transition(Q(50) , Lettre('0') ,Q(51) , Lettre('1') , Left) ;
Transition(Q(50) , Lettre('n') ,Q(51) , Epsilon , Left) ;
Transition(Q(50) , Wildcard ,Q(50) , Wildcard , Right) ;
Transition(Q(51) , Lettre('n') ,Q(40) , Epsilon , Left) ;
Transition(Q(51) , Wildcard ,Q(51) , Wildcard , Left) ;
Transition(Q(90) , Lettre('A') ,Q(91) , Epsilon , Right) ;
Transition(Q(90) , Wildcard ,Q(90) , Wildcard , Left) ;
Transition(Q(91) , Epsilon ,Q(91) , Epsilon , Right) ;
Transition(Q(91) , Wildcard ,Q(99) , Wildcard , None)
];;
