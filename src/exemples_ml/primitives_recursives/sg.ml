let transitions_sg = [ 
Transition(Q(0) , Lettre('1') ,Q(10) , Lettre('1') , Right) ;
Transition(Q(0) , Epsilon ,Q(20) , Epsilon , Left) ;
Transition(Q(0) , Wildcard ,Q(0) , Wildcard , Right) ;
Transition(Q(10) , Epsilon ,Q(11) , Epsilon , Left) ;
Transition(Q(10) , Wildcard ,Q(10) , Wildcard , Right) ;
Transition(Q(11) , Epsilon ,Q(9999) , Lettre('1') , None) ;
Transition(Q(11) , Wildcard ,Q(11) , Epsilon , Left) ;
Transition(Q(20) , Epsilon ,Q(9999) , Lettre('0') , None) ;
Transition(Q(20) , Wildcard ,Q(20) , Epsilon , Left)
];;
