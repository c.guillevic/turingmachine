let transitions_succ = [ 
Transition(Q(0) , Epsilon ,Q(1) , Epsilon , Left) ;
Transition(Q(0) , Wildcard ,Q(0) , Wildcard , Right) ;
Transition(Q(1) , Lettre('0') ,Q(9999) , Lettre('1') , None) ;
Transition(Q(1) , Lettre('1') ,Q(1) , Lettre('0') , Left) ;
Transition(Q(1) , Epsilon ,Q(9999) , Lettre('1') , Left)
];;
