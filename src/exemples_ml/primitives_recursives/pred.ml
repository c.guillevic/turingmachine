let transitions_pred = [ 
Transition(Q(0) , Lettre('1') ,Q(1) , Lettre('1') , Right) ;
Transition(Q(0) , Epsilon ,Q(9999) , Epsilon , Left) ;
Transition(Q(0) , Wildcard ,Q(0) , Wildcard , Right) ;
Transition(Q(1) , Epsilon ,Q(2) , Epsilon , Left) ;
Transition(Q(1) , Wildcard ,Q(1) , Wildcard , Right) ;
Transition(Q(2) , Lettre('1') ,Q(9999) , Lettre('0') , None) ;
Transition(Q(2) , Lettre('0') ,Q(2) , Lettre('1') , Left)
];;
