let transitions_add = [ 
Transition(Q(0) , Lettre('+') ,Q(1) , Lettre('+') , Right) ;
Transition(Q(0) , Wildcard ,Q(0) , Wildcard , Right) ;
Transition(Q(1) , Lettre('1') ,Q(2) , Lettre('1') , Right) ;
Transition(Q(1) , Lettre('0') ,Q(1) , Lettre('0') , Right) ;
Transition(Q(1) , Epsilon ,Q(900) , Epsilon , Left) ;
Transition(Q(2) , Epsilon ,Q(3) , Epsilon , Left) ;
Transition(Q(2) , Wildcard ,Q(2) , Wildcard , Right) ;
Transition(Q(3) , Lettre('0') ,Q(3) , Lettre('1') , Left) ;
Transition(Q(3) , Lettre('1') ,Q(4) , Lettre('0') , Left) ;
Transition(Q(4) , Lettre('+') ,Q(5) , Lettre('+') , Left) ;
Transition(Q(4) , Wildcard ,Q(4) , Wildcard , Left) ;
Transition(Q(5) , Lettre('1') ,Q(5) , Lettre('0') , Left) ;
Transition(Q(5) , Wildcard ,Q(0) , Lettre('1') , Right) ;
Transition(Q(900) , Lettre('+') ,Q(999) , Epsilon , Left) ;
Transition(Q(900) , Wildcard ,Q(900) , Epsilon , Left)
];;
