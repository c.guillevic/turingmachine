(************************************************
Main du projet Machine de Turing
Corentin GUILLEVIC
Jarod BLAIN
*************************************************)

(*On charge les differents fichiers*)
#use "machine_turing.ml";;
#use "parser.ml";;
#use "interface_graphique.ml";;


(*Exemples de chargement des machines de Turing predefinis*)
(*
#use "exemples_ml/divers/evaluateur_booleen.ml";;
#use "exemples_ml/langages/anbncn.ml";;
#use "exemples_ml/primitives_recursives/mul.ml";;
*)

(*utilisation du parser*)
(*
let m1 =  generer_modele "exemples_dat/langages/anbncn.dat";;
let m2 =  generer_modele "exemples_dat/langages/anbm_n<m.dat";;
let m3 =  generer_modele "exemples_dat/langages/WW.dat";;
let m4 =  generer_modele "exemples_dat/langages/WW^r.dat";;

let m5 = generer_modele "exemples_dat/primitives_recursives/add.dat";;
let m6 = generer_modele "exemples_dat/primitives_recursives/pred.dat";;
let m7 = generer_modele "exemples_dat/primitives_recursives/sg.dat";;
let m8 = generer_modele "exemples_dat/primitives_recursives/succ.dat";;
let m9 = generer_modele "exemples_dat/primitives_recursives/mul.dat";;


let m10 =  generer_modele "exemples_dat/divers/occurences.dat";;
let m11 =  generer_modele "exemples_dat/divers/machine_basique_1.dat";;
let m12 =  generer_modele "exemples_dat/divers/chiffrement.dat";;
let m13 =  generer_modele "exemples_dat/divers/double.dat";;
let m14 =  generer_modele "exemples_dat/divers/evaluateur_booleen.dat";;
let m15 =  generer_modele "exemples_dat/divers/evaluateur_booleen_a_variables.dat";;
*)

(*utilisation de l interface console*)
(*
lancer_modele m1 "aaabbbccc";;
lancer_modele m2 "aaabbbb";;
lancer_modele m3 "abaaabaa";;
lancer_modele m4 "baaaaaab";;

lancer_modele m5 "111+10";;
lancer_modele m6 "0";;
lancer_modele m6 "111";;
lancer_modele m7 "111";;
lancer_modele m7 "0";;
lancer_modele m8 "111";;
lancer_modele m9 "1111x10";;

lancer_modele m10 "o/coco rico*";;
lancer_modele m11 "";;
lancer_modele m12 "hghx uqjsxf/ha/yb/nc/pd/be/kf/fg/rh/si/mj/wk/gl/zm/xn/ao/tp/oq/jr/vs/ut/qu/dv/cw/lx/iy/ez";;
lancer_modele m13 "|||";;

lancer_modele m14 "(n(1|0)|n(1&0))&n0";;

XOR :
lancer_jeu_essai m15 "(na&b)|(nb&a):a0/b0";;
lancer_jeu_essai m15 "(na&b)|(nb&a):a1/b0";;
lancer_jeu_essai m15 "(na&b)|(nb&a):a0/b1";;
lancer_jeu_essai m15 "(na&b)|(nb&a):a1/b1";;
*)

(*utilisation de l interface graphique*)
(*
lancer_interface_graphique_automatique m1 "aaabbbccc";;
lancer_interface_graphique_manuelle m5 "11+10";;
lancer_interface_graphique_manuelle m9 "11x10";;

lancer_interface_graphique_manuelle m14 "n(1&0)&(n1|nnn0)";;
*)

