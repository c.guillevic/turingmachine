(************************************************
Implementation du Parser pour Machine de Turing
Corentin GUILLEVIC
Jarod BLAIN
*************************************************)

(*Teste si "str" est un commentaire*)
let est_un_commentaire str = if String.length str > 2 then String.get str 0 = '/' && String.get str 1 = '/' else false;;

(*Teste si "str" est sans caractere (vide ou composee que d espaces)*)
let chaine_est_sans_caractere str =
	let rec aux i = 
		if i < ((String.length str)-1) then
			begin
		 	if (String.get str i) = ' ' then aux (i+1)
			else false
			end
		else true
	in aux 0;;	

(*Retourne une liste des lignes du fichier (sans les commentaires et les lignes vides)*)
let decouper_fichier file = 
	let rec aux l =
		try 
			let line = input_line file in
			flush stdout;
			match line with
			|s when (chaine_est_sans_caractere s) || (est_un_commentaire s) -> aux l
			|_ -> line::(aux l)
		with End_of_file -> l
	in aux [];;
	
(*Decoupe la ligne en liste de chaines de caracteres (selon le caractere ' ')*)
let decouper_ligne ligne = String.split_on_char ' ' ligne;;

(*Supprime les chaînes de caractères vide de la liste*)
let filtrer_ligne_decoupee ligne_decoupee =
	let rec aux l = 
		match l with
		|[] -> []
		|""::f -> aux f
		|d::f -> d::(aux f)
	in aux ligne_decoupee;;  

(*Convertit le fichier dont le chemin est fourni en liste de chaine de caractere*)
let convertir_fichier path = decouper_fichier (open_in path);;

(*Retourne la description d une ligne decoupee*)
let ligne_decoupee_to_string ligne = List.fold_left (fun a b -> a ^ " " ^ b) "" ligne;; 

(*Creer une transition a partir d une liste d elements (liste de 5 chaines de caracteres)*)
let creer_transition liste_elements =
	match liste_elements with
	|[a;b;c;d;e] -> Transition(string_to_etat a, string_to_symbole b, string_to_etat c, string_to_symbole d, string_to_direction e)
	|l -> failwith ("mauvais format de ligne " ^ (ligne_decoupee_to_string l));;
	
(*Convertit une ligne en une transition*)
let parser_ligne ligne = creer_transition (filtrer_ligne_decoupee (decouper_ligne ligne));;
	 
(*Convertit un fichier en une liste de transitions*)
let parser_fichier path = List.map parser_ligne (convertir_fichier path);;

(*Genere une machine de turing a partir du fichier du path*)
let generer_modele path = construire_modele (parser_fichier path);;
