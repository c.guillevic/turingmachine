(*********************************
Implementation Machine de Turing
Corentin GUILLEVIC
Jarod BLAIN
*********************************)

(*********************************
Type Machine de Turing
*********************************)

(*Direction possible pour la tete de lecture*)
type direction = Right | Left | None;; 

(*Symbole d'un alphabet*)
type symbole = Lettre of char | Epsilon | Wildcard;;

type etat = Q of int;;

type transition = Transition of etat * symbole * etat * symbole * direction;; 

(*Represente le quintuplet d une machine de Turing *)
type mt = {mutable etats : etat list ;mutable alphabet : symbole list ;mutable initial : etat ;mutable transitions : transition list ;mutable finaux : etat list}

(*Definition d une case_ruban (modelise un chainage double avec comme element aux extremites Null) *)
type case_ruban = Case of {mutable precedent : case_ruban ; mutable symbole : symbole ; mutable suivant : case_ruban} | Null;;

(*
  Type de notre modele 
  Contient :
    - le type machine de Turing
    - la tete de lecture (un chainage double)
    - l etat courant dans lequel se trouve la machine de Turing
*)
type modele = {mt : mt ; mutable tete_lecture : case_ruban ; mutable etat_courant : etat};;

(*********************************
Fonctions
*********************************)

(*Fonctions pour decouper les transitions*)
let obtenir_etat_avant transition = match transition with Transition(e,_,_,_,_) -> e;;
let obtenir_symbole_avant transition = match transition with Transition(_,l,_,_,_) -> l;;
let obtenir_etat_apres transition = match transition with Transition(_,_,e,_,_) -> e;;
let obtenir_symbole_apres transition = match transition with Transition(_,_,_,l,_) -> l;;
let obtenir_direction transition = match transition with Transition(_,_,_,_,d) -> d;;


(*
  Concatene deux couples de string : 
  ("bon", "sa") ("jour", "lut") -> ("bonjour", "salut")
*)
let concat_couple couple1 couple2 = 
  match couple1 with |(a,b) -> match couple2 with |(c,d) -> (a ^ c , b ^ d);;

(*Teste si elem est present dans une liste en testant l egalite avec la fonction test_egalite*)
(*
Fonction concise mais peu efficace : necessite un double parcours
let est_present_liste test_egalite liste elem = List.fold_left (fun a b -> a || b) false (List.map (test_egalite elem) liste);;
*)
let est_present_liste test_egalite liste elem = 
	let rec aux l = 	
		match l with
		|[] -> false
		|d::f -> (test_egalite elem d) || aux f
	in aux liste;;

(*********************************
Fonctions de converions
*********************************)
(*Convertit un symbole en une chaine de caracteres (utile pour l affichage)*)
let symbole_to_string symbole = 
    match symbole with
    |Epsilon -> " " 
    |Wildcard -> "-"
    |Lettre(l) -> String.make 1 l
;; 


(*
Convertit une chaine de caracteres en Direction :
	Right = "Right" ou "R" ou "Droite" ou "D"
	Left = "Left" ou "L" ou "Gauche" ou "G"
	None = "None" ou "N"
*)
let string_to_direction s = 
	match s with
	|"Right"|"R"|"Droite"|"D" -> Right
	|"Left"|"L"|"Gauche"|"G" -> Left
	|"None"|"N" -> None
	|_ -> failwith "direction incorrecte"
;;

(* 
Convertit une chaine de caracteres en symbole :
	Epsilon = "."
	Wildcard = "-"
	Lettre(x) = Premier caractere de la chaine de caractere 
*)
let string_to_symbole s = 
	match s with
	|"." -> Epsilon
	|"-" -> Wildcard
	|_ -> Lettre(String.get s 0);;

(*Convertit une chaine de caracteres en etat : chaine doit representer un nombre*)
let string_to_etat s = Q(int_of_string s);; 

(*********************************
Tests d egalite
*********************************)

(*Teste si le symbole est epsilon*)
let est_epsilon symbole = 
  match symbole with
  |Epsilon -> true
  |_ -> false;; 

(*Teste si les symboles sont egaux*)
let symboles_egaux s1 s2 = 
  match s1 with
  |Wildcard -> true
  |Epsilon -> est_epsilon s2
  |Lettre(c) -> begin
                  match s2 with
                  |Wildcard -> true
                  |Epsilon -> false
                  |Lettre(c2) -> c = c2
                end
;;

(*Teste si les etats sont egaux*)
let etats_egaux e1 e2 = match e1 with |Q(etat1) -> begin match e2 with |Q(etat2) -> etat1 = etat2 end;;

(*Teste si e1 est inferieur a e2*)
let etat_est_inferieur e1 e2 = match e1 with |Q(etat1) -> begin match e2 with |Q(etat2) -> etat1 < etat2 end;;

(*Teste si e1 est superieur a e2*)
let etat_est_superieur e1 e2 = match e1 with |Q(etat1) -> begin match e2 with |Q(etat2) -> etat1 > etat2 end;;

(*********************************
Fonctions pour le ruban
*********************************)

(*Creer une case de ruban a partir d un symbole, de la case precedente et de la case suivante*)
let creer_case symbole prec suiv = Case({precedent = prec ; suivant = suiv ; symbole = symbole});;

(*Ajoute une case avant "case" a partir du symbole et retourne "case"*)
let ajouter_precedent case symbole = 
  match case with
  | Null -> failwith "case null"
  | Case(x) -> 
      let prec = (creer_case symbole Null (Case(x))) in
      x.precedent <- prec ; Case(x) 
;;

(*Ajoute une case apres "case" a partir du symbole et retourne "case"*)
let ajouter_suivant case symbole = 
  match case with
  | Null -> failwith "case null"
  | Case(x) -> 
      let suivant = (creer_case symbole (Case(x)) Null) in
      x.suivant <- suivant ; Case(x) 
;;

(*Ajoute une case en tete du ruban puis retourne la nouvelle tete du chainage, utile pour l initialisation du ruban*)
let rec ajouter_en_tete case symbole =
    match case with 
    |Null -> creer_case symbole Null Null
    |Case(x) -> match x.precedent with
                |Null -> ajouter_precedent (Case(x)) symbole
                |Case(x) -> match ajouter_en_tete (Case(x)) symbole with
							|Case(c) -> c.precedent
							|_ -> Null
;;

(*Renvoie une chaine de caracteres de la partie a gauche du ruban*)
let rec parcours_gauche_ruban_to_string case_ruban =
  match case_ruban with
  |Null -> "|..."
  |Case(x) -> (parcours_gauche_ruban_to_string x.precedent) ^ "|" ^ (symbole_to_string x.symbole)
;;

(*Renvoie une chaine de caracteres de la partie a droite du ruban*)
let rec parcours_droit_ruban_to_string case_ruban =
  match case_ruban with
  |Null -> "...|"
  |Case(x) ->(symbole_to_string x.symbole) ^ "|" ^ (parcours_droit_ruban_to_string x.suivant)
;;


(*Renvoie une chaine de caracteres du ruban*)
let ruban_to_string case_ruban =
  match case_ruban with
  |Null -> "...|ε|..."
  |Case(x) -> (parcours_gauche_ruban_to_string x.precedent) ^ "|" ^ (symbole_to_string x.symbole) ^ "|" ^ (parcours_droit_ruban_to_string x.suivant)
;;


(*Affiche le ruban dans la console*)
let afficher_ruban_console case_ruban = print_string (ruban_to_string case_ruban);print_newline ();;

(*Decoupe une chaine de caracteres et place chaque caractere dans une case du ruban qu elle renvoit*)
let initialiser_ruban str =
  if str = "" then creer_case Epsilon Null Null
  else
  let rec aux ind ruban = 
    if ind < 0 then ruban
    else aux (ind-1) (ajouter_en_tete ruban (Lettre(String.get str ind)))
  in aux ((String.length str)-1) Null
  ;;

(***********************************
Fonctions pour la machine de Turing
***********************************)

(*Permet de changer la liste des etats finaux de la machine de Turing*)
let changer_etats_finaux mt liste_etats = mt.finaux <- liste_etats;;

(*Permet de changer l etat initial de la machine de Turing*)
let changer_etat_initial mt etat = mt.initial <- etat;;

(*Deplace la tete de lecture a droite, si la case n existe pas : elle cree une case vide (symbole Epsilon) *)
let deplacement_droit tete_lecture =
  match tete_lecture with 
  |Null -> failwith "Case null"
  |Case(x) -> match x.suivant with    
              |Null ->  begin
                          match ajouter_suivant tete_lecture Epsilon with
                          |Null -> failwith "case null"
                          |Case(x) -> x.suivant
                        end
              |Case(x) -> Case(x)
;;

(*Deplace la tete de lecture a gauche, si la case n existe pas : elle cree une case vide (symbole Epsilon) *)
let deplacement_gauche tete_lecture =
  match tete_lecture with 
  |Null -> failwith "Case null"
  |Case(x) -> match x.precedent with    
              |Null ->  begin
                          match ajouter_precedent tete_lecture Epsilon with
                          |Null -> failwith "case null"
                          |Case(x) -> x.precedent
                        end
              |Case(x) -> Case(x)
;;


(*Deplace la tete de lecture ou la laisse sur place selon la direction*)
let deplacer_tete tete_lecture direction =
    match direction with
    |Right -> deplacement_droit tete_lecture
    |Left -> deplacement_gauche tete_lecture
    |None -> tete_lecture
;;

(*Renvoie le symbole inscrit sur la case ou se situe la tete de lecture*)
let lire_tete tete_lecture = 
  match tete_lecture with
  |Null -> failwith "case null"
  |Case(x) -> x.symbole;;

(*Remplace le symbole de la case ou se situe la tete de lecture*)
let ecrire_tete tete_lecture symbole =   
  match tete_lecture with
|Null -> failwith "case null"
|Case(x) -> x.symbole <- symbole;;

(*Change l etat dans lequel la machine de Turing se trouve*)
let changer_etat modele etat = modele.etat_courant <- etat;; (*Verifier si etat existe ?*)

(*Cherche une transition possible en fonction de l etat et du symbole, si aucunes transitions n est possible : arrete l execution*)
let trouver_transition liste_transitions etat symbole =
  let rec aux transitions = 
    match transitions with
    |[] -> failwith "aucunes transitions possibles"
    |d::f -> begin 
              match d with
              |Transition(e1, Wildcard,e2,Wildcard,d) when (etats_egaux etat e1) -> Transition(e1, symbole, e2, symbole, d)  
              |Transition(e1, Wildcard,e2,s2,d) when (etats_egaux etat e1) -> Transition(e1, symbole, e2, s2, d)  
              |Transition(e1,s,e2,Wildcard,d) when (symboles_egaux symbole s) && (etats_egaux etat e1) -> Transition(e1, symbole, e2, symbole, d)
              |Transition(e,s,_,_,_) when (symboles_egaux symbole s) && (etats_egaux etat e) -> d
              |_ -> aux f
            end
  in aux liste_transitions
;;

(*Teste si l etat courant de la machine de Turing est final ou non*)
let est_dans_etat_final modele = est_present_liste etats_egaux  modele.mt.finaux modele.etat_courant;;


(*
  Renvoie un couple de chaine de caracteres de la partie a gauche de la tete de lecture du ruban
  Element de gauche : ruban
  Element de droite : position de la tete de lecture
*)
let gauche_ruban_step_to_string case_ruban = 
  let rec aux case_ruban str_pair =
    match case_ruban with
    |Null ->  concat_couple ("|...","    ") str_pair
    |Case(x) -> aux (x.precedent)  (concat_couple ("|" ^ (symbole_to_string x.symbole),"  ") str_pair)
  in aux case_ruban ("","")  
  ;;

(*
  Renvoie un couple de chaine de caracteres de la partie a droite de la tete de lecture du ruban
  Element de gauche : ruban
  Element de droite : position de la tete de lecture
*)
let droit_ruban_step_to_string case_ruban = 
  let rec aux case_ruban str_pair =
    match case_ruban with
    |Null ->  concat_couple str_pair ("...|","    ")
    |Case(x) -> aux (x.suivant)  (concat_couple str_pair ((symbole_to_string x.symbole) ^ "|" ,"  "))
  in aux case_ruban ("","")  
  ;;

(*
  Renvoie un couple de chaine de caracteres decrivant le ruban
  Element de gauche : ruban
  Element de droite : position de la tete de lecture
*)
let step_to_string tete =
  match tete with
  |Null -> ("...|ε|...", "         ")
  |Case(x) -> concat_couple (concat_couple(gauche_ruban_step_to_string x.precedent) ("|" ^ (symbole_to_string x.symbole) ^ "|"," ^ ")) (droit_ruban_step_to_string x.suivant)
;;

(*Affiche une etape du fonctionnement de la machine de Turing (le ruban et la position de la tete de lecture a une etape de l execution)*)
let afficher_step tete = 
  let res = step_to_string tete in 
  match res with |(a,b) -> print_string a ; print_newline () ; print_string b ; print_newline ();; 


(*
Realise un cycle de la machine de Turing
  - lecture
  - ecriture
  - deplacement
*)
let mouvement modele = 
    let tete = modele.tete_lecture in
    let symbole = lire_tete tete in
    let etat = modele.etat_courant in 
      let transition = trouver_transition modele.mt.transitions etat symbole in
        ecrire_tete tete (obtenir_symbole_apres transition);
        changer_etat modele (obtenir_etat_apres transition);
        modele.tete_lecture <- deplacer_tete tete (obtenir_direction transition)
;;

(*
Realise un cycle de la machine de Turing et affiche le ruban apres l ecriture
  - lecture 
  - ecriture
  - deplacement
*)
let mouvement_affichage modele = 
  let tete = modele.tete_lecture in
  let symbole = lire_tete tete in
  let etat = modele.etat_courant in 
    let transition = trouver_transition modele.mt.transitions etat symbole in
      ecrire_tete tete (obtenir_symbole_apres transition);
      afficher_step tete;
      changer_etat modele (obtenir_etat_apres transition);
      modele.tete_lecture <- deplacer_tete tete (obtenir_direction transition)
;;

(*Ajoute l etat a la liste des etats de la liste d etat si il n y est pas*)
let ajouter_etat liste etat =  
  if not (est_present_liste etats_egaux liste etat) then etat::liste
  else liste;;

(*Ajoute le symbole a la liste des symboles si il n y est pas*)
let ajouter_symbole liste symbole =  
  if not (est_present_liste symboles_egaux liste symbole) then symbole::liste
  else liste;;

  
(*Retourne le plus petit etat de la liste d etats*)
let plus_petit_etat liste_etats = 
  let elem = List.hd liste_etats in
    List.fold_left (fun a b -> if etat_est_inferieur a b then a else b) elem liste_etats;;

    
(*Retourne le plus grand etat de la liste d etats*)
let plus_grand_etat liste_etats = 
  let elem = List.hd liste_etats in
    List.fold_left (fun a b -> if etat_est_superieur a b then a else b) elem liste_etats;;

    
(*Retourne l alphabet et les etats a partir des transitions*)
let alphabet_et_etats transitions = 
  let rec aux alphabet etats l =
    match l with
    |[] -> (alphabet, etats) 
    |d::f ->  let e1 = obtenir_etat_avant d in   
              let e2 = obtenir_etat_apres d in 
              let s1 = obtenir_symbole_avant d in
              let s2 = obtenir_symbole_apres d in
              aux (ajouter_symbole (ajouter_symbole alphabet s1) s2) (ajouter_etat (ajouter_etat etats e1) e2) f
  in aux [] [] transitions;;


(*
Construit le quintuplet d une machine de Turing automatiquement a partir de la liste des transitions
Etat initial = plus petit etat
Etats finaux = plus grand etat 
*)
let construire_machine_turing liste_transitions =
  match alphabet_et_etats liste_transitions with
  |(alphabet, etats) -> {etats = etats ; alphabet = alphabet ; initial = (plus_petit_etat etats) ; transitions = liste_transitions ; finaux = [(plus_grand_etat etats)]};;
    
(*Construit un modele fonctionnelle a partir d une liste de transitions*)
let construire_modele liste_transitions =  let mt = construire_machine_turing liste_transitions in {mt = mt ; tete_lecture = initialiser_ruban ""; etat_courant = mt.initial};;

(*Modifie le ruban d une machine de Turing*)
let modifier_ruban_modele modele mot = modele.tete_lecture <- initialiser_ruban mot;;

(*Remet la machine de Turing dans son etat initial*)
let reinitialiser_modele modele = modele.etat_courant <- modele.mt.initial;;

(******************************************************
Fonctions pour l affichage graphique (Dans la console)
******************************************************)

(*Genere une liste de size string vide *)
let generer_liste_string_vide size = 
	let rec aux n liste =
		if n <= 0 then liste else aux (n-1) (""::liste)
	in aux size [];;	

(*Genere la liste des "size" cases precedant la "case"*)
let generer_liste_string_ruban_gauche case size = 
	let rec aux case n liste =
		if n <= 0 then liste 
		else 		
			match case with
				|Null -> (generer_liste_string_vide n) @ liste
				|Case(x) -> aux x.precedent (n-1) ((symbole_to_string x.symbole)::liste)
	in aux case size []
;;


(*Genere la liste des "size" cases suivant la "case"*)
let generer_liste_string_ruban_droit case size =
	let rec aux case n =
		if n <= 0 then [] 
		else 		
			match case with
				|Null -> generer_liste_string_vide n
				|Case(x) -> ((symbole_to_string x.symbole)::(aux x.suivant (n-1)))
	in aux case size
;;

(*Genere la liste des cases d'un ruban : "left_size" cases a gauche de la tete de lecture, la tete de lecture, "right_size" cases a droit de la tete de lecture*)
let ruban_to_string_list ruban left_size right_size =
	match ruban with
	|Null -> generer_liste_string_vide (1+left_size+right_size)
	|Case(x) -> (generer_liste_string_ruban_gauche x.precedent left_size) @ ([(symbole_to_string x.symbole)] @ (generer_liste_string_ruban_droit x.suivant right_size))
;;

(*Execute des cycles de la machine de Turing jusqu a ce qu elle soit dans un etat final*)
let rec run_modele modele =
  afficher_step modele.tete_lecture;
  if est_dans_etat_final modele then begin print_string "Fin" ; print_newline (); end
  else begin
    mouvement_affichage modele;
    print_newline ();
    run_modele modele;
  end;;

(*Reinitialise le modele, change son ruban et l execute*)  
let lancer_modele modele mot = reinitialiser_modele modele ; modifier_ruban_modele modele mot ; run_modele modele;;

