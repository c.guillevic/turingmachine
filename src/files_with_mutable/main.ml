(************************************************
Main du projet Machine de Turing
Corentin GUILLEVIC
Jarod BLAIN
*************************************************)

(*On charge les differents fichiers*)
#use "machine_turing.ml";;
#use "../parser.ml";;
#use "interface_graphique.ml";;


(*Chargement des machines de Turing predefinis*)
#use "../exemples_ml/langages/anbncn.ml";;

(*utilisation du parser*)
let m1 =  generer_modele "../exemples_dat/langages/anbncn.dat";;

(*utilisation de l interface console*)
lancer_modele m1 "aaabbbccc";;

(*utilisation de l interface graphique*)
lancer_interface_graphique m1 "aaabbbccc";;
