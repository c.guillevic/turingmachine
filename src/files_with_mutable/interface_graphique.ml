(**************************************************************
Implementation de l'interface graphique pour Machine de Turing
Corentin GUILLEVIC
Jarod BLAIN
**************************************************************)
(* Permet de charger le module graphique de Ocaml *)
#load "graphics.cma";;

(* Permet de charger le module Unix de OCaml (comportant la fonction sleep)*)
#load "unix.cma";;

(* Pour rentrer dans le module *)
open Graphics;;

(********************************
Types, classe et exceptions
********************************)

(*Exception pour l'interface graphique*)
exception End;;
exception Not_a_button;;
exception Interface_error of string;;

(* Classe bouton *)
class bouton x y w h (dessin : int->int->int->int->unit) (event : unit->unit) =
  object 
    val mutable x = x 
    val mutable y = y 
    val mutable w = w 
    val mutable h = h
	(*fonction a appeler pour dessiner le bouton*)
    val mutable dessin = dessin
	(*fonction a appeler quand le bouton est clique*)
    val mutable event = event
	(*renvoie vrai quand (mx,my) represente une coordonnee du bouton*)
    method is_target mx my = (mx >= x && mx <= (x+w)) && (my >= y && (my <= (y+h)))
	(*dessine le bouton a l emplacement et a la taille voulu (x, y, w, h)*)
    method se_dessiner = dessin x y w h
	(*declenche l evenement event*)
    method on_click = event ()
	method set_event e = event<-e
	method set_dessin d = dessin<-d
  end;;

(********************************
Boutons
********************************)

(*
Calcule la nouvelle valeur de "valeur"
"valeur" doit etre un pourcentage (0-100)
Convertis la valeur en une valeur comprise entre origine et origine+max
*)
let nouvelle_valeur origine max valeur = origine + ((valeur*max)/100);; 

(*
Calcule les coordonnees d un point donne en pourcentage
point = (0-100 , 0-100)
*)
let calculer_coordonnees x y maxw maxh point= 
	match point with |(a,b) -> ((nouvelle_valeur x maxw a), (nouvelle_valeur y maxh b));;

(*Dessine une liste de polygones en utilisant poly_option (draw_poly ou fill_poly)*)
let dessiner_liste_poly liste poly_option = 
	let rec aux liste = 
		match liste with
		|[] -> ()
		|d::f ->  begin poly_option d; aux f end
	in aux liste
;; 

(*Dessine une croix aux coordonnees x y et de taille w h*)
let dessin_bouton_close x y w h =
	let liste_poly_fill = [[|0,95 ; 0,100 ; 5,100 ; 100,5 ; 100,0 ; 95,0|] ; [|5,0 ; 0,0 ; 0,5 ; 95,100 ; 100,100 ; 100,95|]] in
	set_color black;
	dessiner_liste_poly (List.map (Array.map (calculer_coordonnees x y w h)) liste_poly_fill) fill_poly;
;;

(*Dessine une fleche et une barre aux coordonnees x y et de taille w h*)
let dessin_bouton_step x y w h = 
	let liste_poly_fill_white = [[|0,0 ; 100,0 ; 100,100 ; 0,100|]] in
	let liste_poly_fill_black = [[|20,20 ; 20,80 ; 80,50|] ; [|76,20 ; 76,80 ; 84,80 ; 84,20|]] in
	let liste_poly_draw_black = [[|0,0 ; 100,0 ; 100,100 ; 0,100|]] in
	set_color white;
	dessiner_liste_poly (List.map (Array.map (calculer_coordonnees x y w h)) liste_poly_fill_white) fill_poly;
	set_color black;
	dessiner_liste_poly (List.map (Array.map (calculer_coordonnees x y w h)) liste_poly_fill_black) fill_poly;
	dessiner_liste_poly (List.map (Array.map (calculer_coordonnees x y w h)) liste_poly_draw_black) draw_poly
;;
	
(*Events par defaut*)
let close_event () = raise End;;
let run_event_without_modele () = raise (Interface_error "Aucun modele (run_event)");;
let step_event_without_modele () = raise (Interface_error "Aucun modele (step_event)");;

let bouton_close = new bouton 1180 620 80 80 dessin_bouton_close close_event;;
let bouton_step = new bouton 330 550 80 80 dessin_bouton_step step_event_without_modele;;

(*Liste des boutons*)
let liste_boutons = [bouton_close ; bouton_step];;

(* Affiche la liste des boutons *)
let afficher_boutons () = 
	let rec aux l = 	
		match l with
		|[] -> ()
		|d::f -> d#se_dessiner;aux f
	in aux liste_boutons;;

(* Cherche si un bouton a ete clique ou leve l exception Not_a_button *)
let trouver_boutons_clique x y = 
	let rec aux l = 	
		match l with
		|[] -> raise Not_a_button
		|d::f -> if d#is_target x y then d else aux f
	in aux liste_boutons;;
	
(********************************
Affichage
********************************)

(*Ouvre la fenetre graphique*)
let ouvrir_fenetre () = open_graph " 1280x720";;

(*Colorie le fond de la fenetre graphique*)
let afficher_background color = set_color color; fill_rect 0 0 1280 720;;

(* Afficher les coordonnees reperes *) 
let afficher_repere () = 
	moveto 0 0;
	draw_string "0 0";
	moveto 1230 705;
	draw_string "1280 720";
	moveto 1230 0;
	draw_string "1280 0";
	moveto 0 705;
	draw_string "0 720";
	moveto 0 0;;

(*Affiche la case "action en cours"*)
let afficher_case_action () = 
	set_color white;
	fill_rect 320 130 640 170;
	set_color black;
	draw_rect 320 130 640 170;
	moveto 325 285;
	draw_string "Action en cours :"
;;

(*Affiche le ruban (vide) *)
let afficher_ruban () = 	
	for i = 20 downto 1 do 
		set_color white;
		fill_rect 51 336 ((i*59)-1) 99;
		set_color black;
		draw_rect 50 335 (i*59) 100;
	done
;;


(* Initialise la fenetre graphique *)
let initialiser_fenetre () = 
	begin 
		ouvrir_fenetre ();
		afficher_background (rgb 254 215 84); 
		afficher_case_action ();
		afficher_ruban ();
		(* Déplace le curseur courant *)
		moveto 507 0;
		set_color black;
		draw_string "Projet OCaml Guillevic Corentin et Blain Jarod";
		(*
		set_color white;
		fill_rect 50 335 1180 100;
		*)
		(* Tete de lecture *)
		set_color black;
		fill_poly[|670,450 ; 640,490 ; 700,490|];
		fill_rect 660 490 20 30;
		for i = 1 to 2 do
			draw_rect (640+i) (335+i) (59-2*i) (100-2*i)
		done;
		moveto 627 526;
		draw_string "Tete de lecture"	
	end
;;


(* Les coordonnées auxquelles afficher l'action en cours : 435x285 *)

(* Affichage individuel de contenu de cases *)
let afficher_element string rang =
	set_text_size 40;
	set_color black;
	moveto (75+rang*59) 380;
	draw_string string;;

(* 
Reinitialise et remplis le ruban 
Chacune des 20 cases fait 59px de large, 100 de haut
*)
let remplir_ruban liste_case =
	afficher_ruban ();
	let rec aux l indice = 
		match l with
		|[] -> ()
		|d::f -> afficher_element d indice;aux f (indice+1)
	in aux liste_case 0;; 

(* Retourne la liste de string pour le ruban *)
let get_ruban modele = ruban_to_string_list (modele.tete_lecture) 10 9;;

let mouvement_modele_interface_graphique modele = mouvement modele; remplir_ruban (get_ruban modele);;

(* Event step by step *)
let step_event modele () = 
if not (est_dans_etat_final modele) then 	
	begin
		mouvement modele;	
		remplir_ruban (get_ruban modele)
	end;;

(********************************
Fonction principale 
********************************)

(* Lance l interface graphique en utilisant le "modele" initialiser avec "mot" *)
let lancer_interface_graphique modele mot =
	initialiser_fenetre ();
	afficher_boutons ();
	reinitialiser_modele modele ; 
	modifier_ruban_modele modele mot ;
	remplir_ruban (get_ruban modele);
	bouton_step#set_event (step_event modele);	
	try
		while true do
			let event = Graphics.wait_next_event [Graphics.Button_down] in
				try 
					if event.Graphics.button then
						let bouton = trouver_boutons_clique (event.Graphics.mouse_x) (event.Graphics.mouse_y) in 
						bouton#on_click
				with
				|Not_a_button -> ()
				|Failure(s) -> print_string s; raise End
		done
	with 
	|Interface_error(s) -> print_string s;close_graph ()
	|End -> close_graph ()
	|Graphics.Graphic_failure(_) -> close_graph ();;	

(* Lance l interface graphique en utilisant le "modele" initialiser avec "mot" en mode automatique*)
let lancer_interface_graphique_automatique modele mot =
	initialiser_fenetre ();
	reinitialiser_modele modele ; 
	modifier_ruban_modele modele mot ;
	remplir_ruban (get_ruban modele);
	try
		while true do
			try 
				Unix.sleep 1;
				if (not (est_dans_etat_final modele)) then mouvement_modele_interface_graphique modele
			with
			|Failure(s) -> print_string s; raise End
		done
	with 
	|End -> close_graph ()
	|Graphics.Graphic_failure(_) -> close_graph ();;	
		
