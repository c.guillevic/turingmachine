(*********************************
Implementation Machine de Turing
Corentin GUILLEVIC
Jarod BLAIN
*********************************)

(*********************************
Types Machine de Turing
*********************************)

(*Direction possible pour la tete de lecture*)
type direction = Right | Left | None;; 

type symbole = Lettre of char | Epsilon | Wildcard;;

type etat = Q of int;;

type transition = Transition of etat * symbole * etat * symbole * direction;; 

(*Represente le quintuplet d une machine de Turing *)
type mt = {etats : etat list ;alphabet : symbole list ;initial : etat ;transitions : transition list ;finaux : etat list}

(*Represente une case du ruban infini*)
type case = Case of {symbole : symbole ; suite : case} | Null;;

(*Definition de la tete de lecture*)
type tete = Tete of {precedent : case ; symbole : symbole ; suivant : case};;

(*
  Type de notre modele 
  Contient :
    - le type machine de Turing
    - la tete de lecture (un chainage double)
    - l etat courant dans lequel se trouve la machine de Turing
*)
type modele = {mt : mt ; tete_lecture : tete ; etat_courant : etat};;

(*********************************
Fonctions de converions
*********************************)
(*Convertit un symbole en une chaine de caracteres (utile pour l affichage)*)
let symbole_to_string symbole = 
  match symbole with
  |Epsilon -> " " 
  |Wildcard -> "-"
  |Lettre(l) -> String.make 1 l
;; 

(*
Convertit une chaine de caracteres en Direction :
	Right = "Right" ou "R" ou "Droite" ou "D"
	Left = "Left" ou "L" ou "Gauche" ou "G"
	None = "None" ou "N"
*)
let string_to_direction s = 
  match s with
  |"Right"|"R"|"Droite"|"D" -> Right
  |"Left"|"L"|"Gauche"|"G" -> Left
  |"None"|"N" -> None
  |_ -> failwith "direction incorrecte"
;;

(*
Convertit une direction en chaine de caractères,
destinée à l'affichage
*)
let direction_to_string d =
  match d with
  |Right -> "Droite"
  |Left -> "Gauche"
  |None -> "Aucun"
;;

(*
Convertit un etat en chaine de caractères,
destinée à l'affichage
*)
let etat_to_string etat =
  match etat with
  |Q(a) -> string_of_int a
;;

(* 
Convertit une chaine de caracteres en symbole :
	Epsilon = "."
	Wildcard = "-"
	Lettre(x) = Premier caractere de la chaine de caracteres
Utilise par le parser pour definir les transitions 
*)
let string_to_symbole s = 
  match s with
  |"." -> Epsilon
  |"-" -> Wildcard
  |_ -> Lettre(String.get s 0)
;;
  
(* 
Convertit un caractere en symbole :
	Epsilon = ' '
	Lettre(x) : tous les autres caracteres
Utilise pour initialiser le ruban a partir d une chaine de caracteres 
*)
let caractere_to_symbole c =
	match c with
	|' '-> Epsilon
	|_ -> Lettre(c);; 

(*Convertit une chaine de caracteres en etat : chaine doit representer un nombre*)
let string_to_etat s = Q(int_of_string s);; 

(*********************************
Tests d egalite
*********************************)

(*Teste si le symbole est epsilon*)
let est_epsilon symbole = 
  match symbole with
  |Epsilon -> true
  |_ -> false;; 

(*Teste si les symboles sont egaux*)
let symboles_egaux s1 s2 = 
  match s1 with
  |Wildcard -> true
  |Epsilon -> est_epsilon s2
  |Lettre(c) -> begin
      match s2 with
      |Wildcard -> true
      |Epsilon -> false
      |Lettre(c2) -> c = c2
    end
;;

(*Teste si les etats sont egaux*)
let etats_egaux e1 e2 = match e1 with |Q(etat1) -> begin match e2 with |Q(etat2) -> etat1 = etat2 end;;

(*Teste si e1 est inferieur a e2*)
let etat_est_inferieur e1 e2 = match e1 with |Q(etat1) -> begin match e2 with |Q(etat2) -> etat1 < etat2 end;;

(*Teste si e1 est superieur a e2*)
let etat_est_superieur e1 e2 = match e1 with |Q(etat1) -> begin match e2 with |Q(etat2) -> etat1 > etat2 end;;

(*********************************
Fonctions auxiliaires
*********************************)

(*Fonctions pour decouper les transitions*)
let obtenir_etat_avant transition = match transition with Transition(e,_,_,_,_) -> e;;
let obtenir_symbole_avant transition = match transition with Transition(_,l,_,_,_) -> l;;
let obtenir_etat_apres transition = match transition with Transition(_,_,e,_,_) -> e;;
let obtenir_symbole_apres transition = match transition with Transition(_,_,_,l,_) -> l;;
let obtenir_direction transition = match transition with Transition(_,_,_,_,d) -> d;;


(*
  Concatene deux couples de string : 
  ("bon", "sa") ("jour", "lut") -> ("bonjour", "salut")
*)
let concat_couple couple1 couple2 = 
  match couple1 with |(a,b) -> match couple2 with |(c,d) -> (a ^ c , b ^ d);;

(*Teste si elem est present dans une liste en testant l egalite avec la fonction test_egalite*)
(*
Fonction concise mais peu efficace (necessite un double parcours) :
let est_present_liste test_egalite liste elem = List.fold_left (fun a b -> a || b) false (List.map (test_egalite elem) liste);;
*)
let est_present_liste test_egalite liste elem = 
  let rec aux l = 	
    match l with
    |[] -> false
    |d::f -> (test_egalite elem d) || aux f
  in aux liste;;


(*Ajoute l etat a la liste des etats de la liste d etats si il n y est pas*)
let ajouter_etat liste etat =  
  if not (est_present_liste etats_egaux liste etat) then etat::liste
  else liste;;

(*Ajoute le symbole a la liste des symboles si il n y est pas*)
let ajouter_symbole liste symbole = 
  	match symbole with
 	|Wildcard -> liste
	|_ -> if not (est_present_liste symboles_egaux liste symbole) then symbole::liste
  		  else liste
;;

(*Retourne l alphabet et les etats a partir des transitions*)
let alphabet_et_etats transitions = 
  let rec aux alphabet etats l =
    match l with
    |[] -> (alphabet, etats) 
    |d::f ->  let e1 = obtenir_etat_avant d in   
        let e2 = obtenir_etat_apres d in 
        let s1 = obtenir_symbole_avant d in
        let s2 = obtenir_symbole_apres d in
        aux (ajouter_symbole (ajouter_symbole alphabet s1) s2) (ajouter_etat (ajouter_etat etats e1) e2) f
  in aux [] [] transitions;;
  
(*Retourne le plus petit etat de la liste d etats*)
let plus_petit_etat liste_etats = 
  let elem = List.hd liste_etats in
  List.fold_left (fun a b -> if etat_est_inferieur a b then a else b) elem liste_etats;;

    
(*Retourne le plus grand etat de la liste d etats*)
let plus_grand_etat liste_etats = 
  let elem = List.hd liste_etats in
  List.fold_left (fun a b -> if etat_est_superieur a b then a else b) elem liste_etats;;

(*********************************
Fonctions pour le ruban
*********************************)

(*Creer une case de ruban a partir d un symbole et de la case suivante*)
let creer_case symbole suite = Case({symbole = symbole ; suite = suite});;

(*Creer une tete de ruban a partir d un symbole, de la case precedente et de la case suivante*)
let creer_tete symbole prec suiv = Tete({precedent = prec ; suivant = suiv ; symbole = symbole});;

(*Ajoute une case en tete du ruban puis retourne la nouvelle tete du ruban, utile pour l initialisation du ruban*)
let rec ajouter_en_tete tete_lecture symbole = match tete_lecture with|Tete(t) ->  creer_tete symbole Null (creer_case t.symbole t.suivant);;

(*Renvoie une chaine de caracteres de la partie a gauche du ruban*)
let rec parcours_gauche_ruban_to_string case_ruban =
  match case_ruban with
  |Null -> "|..."
  |Case(x) -> (parcours_gauche_ruban_to_string x.suite) ^ "|" ^ (symbole_to_string x.symbole)
;;

(*Renvoie une chaine de caracteres de la partie a droite du ruban*)
let rec parcours_droit_ruban_to_string case_ruban =
  match case_ruban with
  |Null -> "...|"
  |Case(x) ->(symbole_to_string x.symbole) ^ "|" ^ (parcours_droit_ruban_to_string x.suite)
;;

(*Renvoie une chaine de caracteres decrivant le ruban*)
let ruban_to_string tete_lecture = match tete_lecture with |Tete(x) -> (parcours_gauche_ruban_to_string x.precedent) ^ "|" ^ (symbole_to_string x.symbole) ^ "|" ^ (parcours_droit_ruban_to_string x.suivant);;

(*Affiche le ruban dans la console*)
let afficher_ruban_console tete_lecture = print_string (ruban_to_string tete_lecture);print_newline ();;

(*Decoupe une chaine de caracteres et place chaque caractere dans une case du ruban qu elle renvoit*)
let initialiser_ruban str =
  if str = "" then creer_tete Epsilon Null Null
  else
    let rec aux ind ruban = 
      if ind < 0 then ruban
      else aux (ind-1) (ajouter_en_tete ruban (caractere_to_symbole (String.get str ind)))
    in aux ((String.length str)-1) (creer_tete Epsilon Null Null)
;;

(***********************************
Fonctions pour la machine de Turing
***********************************)

(*Modification d une machine de Turing*)

(*Permet de changer la liste des etats finaux de la machine de Turing*)
let changer_etats_finaux mt liste_etats = {etats = mt.etats ; alphabet = mt.alphabet ; initial = mt.initial ; transitions = mt.transitions ; finaux = liste_etats};;

(*Permet de changer l etat initial de la machine de Turing*)
let changer_etat_initial mt etat = {etats = mt.etats ; alphabet = mt.alphabet ; initial = etat ; transitions = mt.transitions ; finaux = mt.finaux};;

(*Change l etat dans lequel la machine de Turing se trouve*)
let changer_etat modele etat = {mt = modele.mt ; tete_lecture = modele.tete_lecture ; etat_courant = etat};;
                           
(*Change la tete de lecture de la machine de Turing*)
let changer_tete_lecture modele tete = {mt = modele.mt ; tete_lecture = tete ; etat_courant = modele.etat_courant};;

(*Change la machine de Turing du modele *)
let changer_mt modele mt = {mt = mt ; tete_lecture = modele.tete_lecture ; etat_courant = modele.etat_courant};; 

(*Change le ruban d une machine de Turing*)
let changer_ruban_modele modele mot = {mt = modele.mt ; tete_lecture = initialiser_ruban mot ; etat_courant = modele.etat_courant};;

(*Remet la machine de Turing dans son etat initial*)
let reinitialiser_modele modele = changer_etat modele modele.mt.initial;;

(*Tete de lecture*)

(*Deplace la tete de lecture a droite, si la case n existe pas : elle cree une case vide (symbole Epsilon) *)
let deplacement_droit tete_lecture =
	match tete_lecture with 
	|Tete(tete) ->	match tete.suivant with    
					|Null -> creer_tete Epsilon (creer_case tete.symbole tete.precedent) Null
					|Case(case_droite) -> creer_tete case_droite.symbole (creer_case tete.symbole tete.precedent) case_droite.suite
;;

(*Deplace la tete de lecture a gauche, si la case n existe pas : elle cree une case vide (symbole Epsilon) *)
let deplacement_gauche tete_lecture =
	match tete_lecture with 
	|Tete(tete) ->  match tete.precedent with    
					|Null -> creer_tete Epsilon Null (creer_case tete.symbole tete.suivant)
					|Case(case_gauche) -> creer_tete case_gauche.symbole case_gauche.suite (creer_case tete.symbole tete.suivant) 
;;


(*Retourne la tete de lecture deplacee*)
let deplacer_tete tete_lecture direction =
  match direction with
  |Right -> deplacement_droit tete_lecture
  |Left -> deplacement_gauche tete_lecture
  |None -> tete_lecture
;;

(*Renvoie le symbole inscrit sur la case ou se situe la tete de lecture*)
let lire_tete tete_lecture =  match tete_lecture with|Tete(x) -> x.symbole;;

(*Remplace le symbole de la case ou se situe la tete de lecture*)
let ecrire_tete tete_lecture symbole = match tete_lecture with|Tete(x) -> creer_tete symbole x.precedent x.suivant;;

(*Autres fonctions*)
                             
(*Cherche une transition possible en fonction de l etat et du symbole, si aucune transition n est possible : leve l exception Failure("aucune transition possible")*)
let trouver_transition liste_transitions etat symbole =
  let rec aux transitions = 
    match transitions with
    |[] -> failwith "aucune transition possible"
    |d::f -> begin 
        match d with
        |Transition(e1, Wildcard,e2,Wildcard,d) when (etats_egaux etat e1) -> Transition(e1, symbole, e2, symbole, d)  
        |Transition(e1, Wildcard,e2,s2,d) when (etats_egaux etat e1) -> Transition(e1, symbole, e2, s2, d)  
        |Transition(e1,s,e2,Wildcard,d) when (symboles_egaux symbole s) && (etats_egaux etat e1) -> Transition(e1, symbole, e2, symbole, d)
        |Transition(e,s,_,_,_) when (symboles_egaux symbole s) && (etats_egaux etat e) -> d
        |_ -> aux f
      end
  in aux liste_transitions
;;

(*Teste si l etat courant de la machine de Turing est final ou non*)
let est_dans_etat_final modele = est_present_liste etats_egaux  modele.mt.finaux modele.etat_courant;;

(******************************************************
Fonctions pour l affichage graphique (Dans la console)
******************************************************)

(*
  Renvoie un couple de chaine de caracteres de la partie a gauche de la tete de lecture du ruban
  Element de gauche : ruban
  Element de droite : position de la tete de lecture
*)
let gauche_ruban_step_to_string case_ruban = 
  let rec aux case_ruban str_pair =
    match case_ruban with
    |Null ->  concat_couple ("|...","    ") str_pair
    |Case(x) -> aux (x.suite)  (concat_couple ("|" ^ (symbole_to_string x.symbole),"  ") str_pair)
  in aux case_ruban ("","")  
;;

(*
  Renvoie un couple de chaine de caracteres de la partie a droite de la tete de lecture du ruban
  Element de gauche : ruban
  Element de droite : position de la tete de lecture
*)
let droit_ruban_step_to_string case_ruban = 
  let rec aux case_ruban str_pair =
    match case_ruban with
    |Null ->  concat_couple str_pair ("...|","    ")
    |Case(x) -> aux (x.suite)  (concat_couple str_pair ((symbole_to_string x.symbole) ^ "|" ,"  "))
  in aux case_ruban ("","")  
;;

(*
  Renvoie un couple de chaine de caracteres decrivant le ruban
  Element de gauche : ruban
  Element de droite : position de la tete de lecture
*)
let step_to_string tete =
	match tete with	|Tete(x) -> concat_couple (concat_couple(gauche_ruban_step_to_string x.precedent) ("|" ^ (symbole_to_string x.symbole) ^ "|"," ^ ")) (droit_ruban_step_to_string x.suivant)
;;

(*Affiche une etape du fonctionnement de la machine de Turing (le ruban et la position de la tete de lecture a une etape de l execution)*)
let afficher_step tete = match step_to_string tete with |(a,b) -> print_string a ; print_newline () ; print_string b ; print_newline ();; 

(******************************************************
Fonctions principales
******************************************************)

(*
Realise un cycle de la machine de Turing
  - lecture
  - ecriture
  - deplacement
*)
let mouvement modele = 
  let tete = modele.tete_lecture in
  let symbole = lire_tete tete in
  let etat = modele.etat_courant in 
	  let transition = trouver_transition modele.mt.transitions etat symbole in
		  let mt_apres_changement_etat = changer_etat modele (obtenir_etat_apres transition) in
		  let nouvelle_tete = deplacer_tete (ecrire_tete tete (obtenir_symbole_apres transition)) (obtenir_direction transition) in
		  changer_tete_lecture mt_apres_changement_etat nouvelle_tete
;;

(*
Realise un cycle de la machine de Turing et affiche le ruban apres l ecriture
  - lecture 
  - ecriture
  - deplacement
*)
let mouvement_affichage modele = 
  let tete = modele.tete_lecture in
  let symbole = lire_tete tete in
  let etat = modele.etat_courant in 
	  let transition = trouver_transition modele.mt.transitions etat symbole in
		  let mt_apres_changement_etat = changer_etat modele (obtenir_etat_apres transition) in
		  let nouvelle_tete = deplacer_tete (ecrire_tete tete (obtenir_symbole_apres transition)) (obtenir_direction transition) in
		  afficher_step tete;
		  changer_tete_lecture mt_apres_changement_etat nouvelle_tete
;;

(*
Construit le quintuplet d une machine de Turing automatiquement a partir de la liste des transitions
Etat initial = plus petit etat
Etats finaux = plus grand etat 
*)
let construire_machine_turing liste_transitions =
  match alphabet_et_etats liste_transitions with
  |(alphabet, etats) -> {etats = etats ; alphabet = alphabet ; initial = (plus_petit_etat etats) ; transitions = liste_transitions ; finaux = [(plus_grand_etat etats)]};;
    
(*Construit un modele fonctionnelle a partir d une liste de transitions*)
let construire_modele liste_transitions =  let mt = construire_machine_turing liste_transitions in {mt = mt ; tete_lecture = creer_tete Epsilon Null Null ; etat_courant = mt.initial};;


(*Execute des cycles de la machine de Turing jusqu a ce qu elle soit dans un etat final et realise un affichage a chaque etape*)
let rec run_modele modele =
  afficher_step modele.tete_lecture;
  if est_dans_etat_final modele then begin print_string "Fin" ; print_newline (); end
  else begin
    let m = mouvement_affichage modele in
    print_newline ();
    run_modele m
  end;;

(*Reinitialise le modele, change son ruban et l execute*)  
let lancer_modele modele mot = run_modele (changer_ruban_modele (reinitialiser_modele modele) mot);;

(******************************************************
Fonctions pour jeux d essai
******************************************************)

(*Execute des cycles jusqu a etre dans un etal final puis affiche le ruban*)
let rec run_jeu_essai modele = 
	if est_dans_etat_final modele then afficher_step modele.tete_lecture
	else run_jeu_essai (mouvement modele);;
	

(*Reinitialise le modele, change son ruban et affiche l etat final de son ruban*)  
let lancer_jeu_essai modele mot = run_jeu_essai (changer_ruban_modele (reinitialiser_modele modele) mot);;
