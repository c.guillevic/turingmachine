(************************************************
Implementation du parser pour caracteres
Corentin GUILLEVIC
Jarod BLAIN
*************************************************)

(*Permet d ecrire dans un fichier*)
open Printf;;

(*Supprime les decimales des points*)
let supprimer_decimal str = List.hd (String.split_on_char '.' str);;

(*Transforme deux chaines de caracteres en couple d entiers*)
let transformation_points a b = ((int_of_string a), (int_of_string b));;

(*Adapte l ordonnees d un point au positionnement de Graphics*)
let convertir_y couple = match couple with |(a,b) -> (a, (100-b));;
  
(*Fonctions permettant de trouver le minimum et le maximum des abscisses et des ordonnees d une liste de points*)

let max_x liste_couple =
  let elem = List.hd liste_couple in
  let max = List.fold_left (fun (a,b) (c,d) -> if a > c then (a,b) else (c,d)) elem liste_couple in
  match max with|(a,_) -> a;;
  
let min_x liste_couple =
  let elem = List.hd liste_couple in
  let min = List.fold_left (fun (a,b) (c,d) -> if a < c then (a,b) else (c,d)) elem liste_couple in
  match min with|(a,_) -> a;;

let max_y liste_couple =
  let elem = List.hd liste_couple in
  let max = List.fold_left (fun (a,b) (c,d) -> if b > d then (a,b) else (c,d)) elem liste_couple in
  match max with|(_,a) -> a;;
  
let min_y liste_couple =
  let elem = List.hd liste_couple in
  let min = List.fold_left (fun (a,b) (c,d) -> if b < d then (a,b) else (c,d)) elem liste_couple in
  match min with|(_,a) -> a;;
 
(*Centrage des formes*) 
let centrer_formes liste_couple = 
 	let decalage_x = 50 - (((max_x liste_couple) - (min_x liste_couple))/2) in
 	let decalage_y= 50 - (((max_y liste_couple) - (min_y liste_couple))/2) in
  List.map (fun (a,b) -> (a+decalage_x, b-decalage_y)) liste_couple;;

(*Retourne un tableau de points a partir d une ligne*)
let transformer_polysvg_to_poly ligne =
  let liste_points = List.map supprimer_decimal (String.split_on_char ' ' ligne) in 
  let rec aux l l2=
    match l with
    |[] -> l2
    |a::b::f -> aux f l2@[(transformation_points a b)]
    |_ -> failwith "liste impaire"
  in   
  let liste_coordonnees = aux liste_points [] in
  Array.of_list (centrer_formes (List.map convertir_y liste_coordonnees))
;;



(*Teste si "str" est un commentaire*)
let est_un_commentaire str = if String.length str > 2 then String.get str 0 = '/' && String.get str 1 = '/' else false;;

(*Teste si "str" est sans caractere (vide ou composee que d espaces)*)
let chaine_est_sans_caractere str =
	let rec aux i = 
		if i < ((String.length str)-1) then
			begin
		 	if (String.get str i) = ' ' then aux (i+1)
			else false
			end
		else true
	in aux 0;;	

(*Retourne une liste des lignes du fichier (sans les commentaires et les lignes vides)*)
let decouper_fichier file = 
	let rec aux l =
		try 
			let line = input_line file in
			flush stdout;
			match line with
			|s when (chaine_est_sans_caractere s) || (est_un_commentaire s) -> aux l
			|_ -> line::(aux l)
		with End_of_file -> l
	in aux [];;
	
(*Separe le caractere et la forme*)
let separer_lettre_forme ligne = String.split_on_char '@' ligne;;

(*Separe les polygones*)
let separer_formes_composees str_forme = String.split_on_char ',' str_forme;;

(*Convertit un point en chaine de caracteres*)
let convertir_point_to_str (a,b) = "(" ^ (string_of_int a) ^ "," ^ (string_of_int b) ^ ")";;

(*Convertit un tableau de points en chaine de caracteres*)
let convertir_array_point_to_str tableau = let l = (Array.to_list tableau) in "[|" ^ (String.concat ";" l) ^ "|]";;

(*Convertit une ligne decrivant un ensemble de points (sous forme x1 y1 x2 y2 ..) en une chaine de caracteres decrivant une tableau de points (sous forme (x1,y1);(x2,y2);..)*)
let convertir_str_forme_to_str_array str_forme = convertir_array_point_to_str (Array.map convertir_point_to_str (transformer_polysvg_to_poly str_forme));; 

(*Convertit une ligne decrivant une forme (plusieurs polygones) en une chaine de caracteres decrivant une liste de tableaux de points*)
let convertir_string_to_str_array_poly chaine_formes_composees =
	let liste_formes = separer_formes_composees chaine_formes_composees in
	"[" ^ String.concat " ; " (List.map convertir_str_forme_to_str_array liste_formes) ^ "]"
;;

(*Genere une chaine de caracteres decrivant une option d un match de la forme "|caractere->liste de tableaux de points" ou leve une exception en cas d erreur (et affiche la ligne ayant cause l erreur)*)
let generer_ligne_pour_match ligne =
	try 	
		match separer_lettre_forme ligne with
	 	|a::b::_ -> "|'" ^ (String.sub a 0 1) ^ "'->" ^ (convertir_string_to_str_array_poly b)
		|_ -> failwith "ligne incomplete"
	with
	|Failure(c) -> failwith (c ^ ligne)
;;

(*Convertit le fichier dont le chemin est fourni en liste de chaine de caracteres*)
let convertir_fichier path = decouper_fichier (open_in path);;
	 
(*Convertit un fichier en une liste d options de match de la forme "|caractere->liste de tableaux de points" *)
let parser_fichier path = List.map generer_ligne_pour_match (convertir_fichier path);;


(*Creer le fichier specifie par path*)
let creer_fichier path = open_out path;;

(*Convertit le fichier "path_src" et enregistre le resultat dans "path_dest"*)
let convertir_fichier_symbole path_src path_dest  =
	let fic = creer_fichier path_dest in 
	let liste_lignes = parser_fichier path_src in
	let rec aux liste =
		match liste with
		|[] -> close_out fic
		|d::f -> fprintf fic "%s\n" d  ; aux f
	in aux liste_lignes 
;; 
