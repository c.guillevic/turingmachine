(**************************************************************
Ce fichier s'occupe de charger les fichiers nécessaires ou 
utiles pour le convertisseur
Corentin GUILLEVIC
Jarod BLAIN
**************************************************************)

#use "../machine_turing.ml";;
#use "../parser.ml";;
#use "convertisseur_dat_to_ml.ml";;

(*
Exemples d utilisation :
	creer_exemples_ml pathNouveauFichier.ml (parser_fichier pathFichier.dat) nom_variable;;
*)
