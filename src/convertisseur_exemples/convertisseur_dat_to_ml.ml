(**************************************************************
Implementation du convertisseur d'exemples
Permet de convertir une liste de transitions d une 
machine de Turing en .dat en un .ml
Corentin GUILLEVIC
Jarod BLAIN
**************************************************************)

(*Permet d ecrire dans un fichier*)
open Printf;;

(*Creer le fichier specifie par path*)
let creer_fichier path = open_out path;;

(*Convertit un symbole en code ocaml(chaine de caracteres)*)
let convertir_symbole_en_ocaml symbole =
	match symbole with
	|Epsilon -> "Epsilon"
	|Wildcard -> "Wildcard"
	|Lettre(c) -> "Lettre('" ^ (String.make 1 c) ^"')"
;;

(*Convertit un etat en code ocaml*)
let convertir_etat_en_ocaml etat = match etat with|Q(x) -> "Q(" ^ (string_of_int x) ^ ")";;

(*Convertit une direction en code ocaml*)
let convertir_direction_en_ocaml direction = 
	match direction with
	|Right -> "Right"
	|Left -> "Left"
	|None -> "None"
;;

(*Convertit une transition en code ocaml*) 
let convertir_transition_en_ocaml transition = 
	match transition with
	|Transition(a,b,c,d,e) -> "Transition(" ^ (convertir_etat_en_ocaml a) ^ " , " ^ (convertir_symbole_en_ocaml b) ^ " ,"  ^  (convertir_etat_en_ocaml c) ^ " , " ^  (convertir_symbole_en_ocaml d) ^ " , " ^  (convertir_direction_en_ocaml e) ^ ")";;


(*Convertit une liste de transitions en un variable dans un fichier ocaml*) 
let creer_exemples_ml path liste_transitions nom_variable =
	let fic = creer_fichier path in 
	fprintf fic "%s\n" ("let " ^ nom_variable ^ " = [ ");
	let rec aux liste =
		match liste with
		|[] -> fprintf fic "%s\n" "];;" ; close_out fic
		|d::[] -> fprintf fic "%s\n" (convertir_transition_en_ocaml d); aux []
		|d::f -> fprintf fic "%s\n" ((convertir_transition_en_ocaml d)^" ;"); aux f
	in aux liste_transitions 
;; 
