(**************************************************************
Implementation de l'interface graphique pour Machine de Turing
Corentin GUILLEVIC
Jarod BLAIN
**************************************************************)
(* Permet de charger le module graphique de OCaml *)
#load "graphics.cma";;

(* Permet de charger le module Unix de OCaml (comportant la fonction sleep)*)
#load "unix.cma";;


(* Pour rentrer dans le module *)
open Graphics;;

(********************************
Types, classe et exceptions
********************************)

(*Exception pour l'interface graphique*)
exception End;;
exception Stop;;
exception Undefined_character;;

(*x,y : coordonnees ; w : width ; h : height*)
type bouton = {x : int ; y : int ; w : int ; h : int};;

(******************************************************
Fonctions pour convertir le ruban d une machine en une
liste compatible avec l interface
******************************************************)

(*Genere une liste de size string vide *)
let generer_liste_string_vide size = 
  let rec aux n liste =
    if n <= 0 then liste else aux (n-1) (""::liste)
  in aux size [];;	

(*Genere la liste des "size" cases precedant la "case"*)
let generer_liste_string_ruban_gauche case size = 
	let rec aux case n liste =
		if n <= 0 then liste 
		else 		
			match case with
			|Null -> (generer_liste_string_vide n) @ liste
			|Case(x) -> aux x.suite (n-1) ((symbole_to_string x.symbole)::liste)
  in aux case size []
;;

(*Genere la liste des "size" cases suivant la "case"*)
let generer_liste_string_ruban_droit case size =
  	let rec aux case n =
		if n <= 0 then [] 
		else 		
			match case with
			|Null -> generer_liste_string_vide n
			|Case(x) -> ((symbole_to_string x.symbole)::(aux x.suite (n-1)))
  in aux case size
;;

(*Genere la liste des cases d'un ruban : "left_size" cases a gauche de la tete de lecture, la tete de lecture, "right_size" cases a droit de la tete de lecture*)
let ruban_to_string_list ruban left_size right_size = match ruban with|Tete(x) -> (generer_liste_string_ruban_gauche x.precedent left_size) @ ([(symbole_to_string x.symbole)] @ (generer_liste_string_ruban_droit x.suivant right_size));;

(* Retourne la liste de string pour le ruban *)
let get_ruban modele = ruban_to_string_list (modele.tete_lecture) 10 9;;


(********************************
Fonctions de dessin
********************************)

(*
Calcule la nouvelle valeur de "valeur"
"valeur" doit etre un pourcentage (0-100)
Convertit la valeur en une valeur comprise entre origine et origine+max
*)
let nouvelle_valeur origine max valeur = origine + ((valeur*max)/100);; 

(*
Calcule les coordonnees d un point donne en pourcentage
point = (0-100 , 0-100)
*)
let calculer_coordonnees x y maxw maxh point= 
	match point with |(a,b) -> ((nouvelle_valeur x maxw a), (nouvelle_valeur y maxh b));;

(*Dessine une liste de polygones en utilisant poly_option (draw_poly ou fill_poly)*)
let dessiner_liste_poly liste poly_option = 
	let rec aux liste = 
		match liste with
		|[] -> ()
		|d::f ->  begin poly_option d; aux f end
	in aux liste
;; 

(********************************
Fonctions pour les boutons
********************************)

(*Dessine une fleche et une barre aux coordonnees x y et de taille w h*)
let dessin_bouton_step x y w h = 
	let liste_poly_fill_white = [[|0,0 ; 100,0 ; 100,100 ; 0,100|]] in
	let liste_poly_fill_black = [[|20,20 ; 20,80 ; 80,50|] ; [|76,20 ; 76,80 ; 84,80 ; 84,20|]] in
	let liste_poly_draw_black = [[|0,0 ; 100,0 ; 100,100 ; 0,100|]] in
	set_color white;
	dessiner_liste_poly (List.map (Array.map (calculer_coordonnees x y w h)) liste_poly_fill_white) fill_poly;
	set_color black;
	dessiner_liste_poly (List.map (Array.map (calculer_coordonnees x y w h)) liste_poly_fill_black) fill_poly;
	dessiner_liste_poly (List.map (Array.map (calculer_coordonnees x y w h)) liste_poly_draw_black) draw_poly
;;



(*Coordonnees et taille du bouton step*)	
let bouton_step = {x = 330 ; y = 550 ; w = 80 ; h = 80};; 

(*Dessine "dessin" aux coordonnees et a la taille de "bouton"*)	
let dessiner_bouton bouton dessin = dessin bouton.x bouton.y bouton.w bouton.h;;

(*Renvoie vrai si x et y correspondent aux coordonnees du "bouton"*)	
let is_target bouton x y = (x >= bouton.x && x <= (bouton.x+bouton.w)) && (y >= bouton.y && (y <= (bouton.y+bouton.h)));;

(**********************************
Dessin de caractères dans le ruban
**********************************)

(*
Convertit un caractere en une liste de tableaux de points correspondant a la forme du caractere
Leve une exception si la forme du caractere n est pas definie
*)
let caractere_to_liste_poly c = 
	match c with 
	|'a'->[[|(68, 26); (68, 34); (60, 27); (51, 25); (44, 27); (41, 33); (42, 38); (46, 40); (53, 42); (68, 45); (68, 45); (68, 52); (52, 49); (44, 48); (38, 45); (33, 40); (32, 33); (36, 22); (49, 18); (59, 20); (68, 26)|] ; [|(74, 19); (72, 26); (72, 41); (72, 53); (71, 61); (68, 67); (62, 71); (51, 72); (40, 71); (32, 65); (28, 56); (37, 55); (41, 63); (50, 65);  (60, 62); (63, 55); (63, 52); (63, 45); (63, 34); (63, 26); (65, 19);  (74, 19)|]]
	|'b'->[[|(78, 44); (76, 54); (72, 63); (65, 68); (56, 70); (41, 63); (41, 88); (33, 88); (33, 43); (41, 43); (45, 58); (55, 63); (65, 58); (69, 43);  (78, 44)|]; [|(78, 44); (69, 43); (64, 28); (54, 23); (43, 30); (41, 43); (33, 43);  (33, 17); (41, 17); (41, 23); (55, 16); (71, 23); (78, 44)|]]
	|'c'->[[|(73, 40); (64, 41); (60, 32); (51, 28); (41, 33); (37, 48); (41, 63); (52, 68); (59, 66); (63, 58); (72, 59); (65, 71); (51, 75); (39, 72);  (30, 63); (28, 48); (34, 28); (51, 21); (65, 26); (73, 40)|]]
	|'d'->[[|(73, 42); (65, 42); (60, 28); (51, 23); (41, 28); (37, 43); (28, 43); (30, 29); (38, 19); (50, 16); (64, 23); (64, 17); (73, 17); (73, 42)|] ; [|(73, 88); (64, 88); (64, 63); (58, 68); (49, 70); (38, 67); (30, 57);  (28, 43); (37, 43); (41, 58); (50, 63); (60, 58); (65, 42); (73, 42);  (73, 88)|]]
	|'e'->[[|(74, 54); (69, 68); (52, 75); (34, 68); (29, 54); (37, 53); (42, 64); (52, 68); (63, 63); (66, 53); (74, 54)|] ; [|(74, 46); (74, 48); (73, 54); (65, 53); (36, 53); (28, 54); (27, 48);  (33, 28); (51, 21); (66, 26); (74, 38); (65, 39); (60, 31); (51, 28);  (40, 33); (36, 46); (74, 46)|]]
	|'f'->[[|(52, 69); (54, 76); (59, 77); (65, 77); (66, 85); (58, 85); (49, 83); (44, 78); (43, 70); (43, 65); (36, 65); (36, 58); (43, 58); (43, 13);  (52, 13); (52, 58); (62, 58); (62, 65); (52, 65); (52, 69)|]]
	|'g'->[[|(45, 70); (55, 75); (64, 71); (64, 77); (54, 82); (42, 79); (34, 69); (32, 56); (37, 37); (54, 29); (64, 34); (64, 41); (55, 37); (45, 41); (41, 56); (45, 70)|]; [|(71, 79); (63, 79); (63, 73); (58, 75); (58, 69); (59, 68); (63, 54); (59, 39); (58, 39); (58, 32); (62, 34); (62, 23); (57, 16); (48, 14); (39, 16); (36, 22); (27, 23); (33, 11); (48, 6); (61, 9); (69, 17); (71, 34); (71, 79)|]]
	|'h'->[[|(72, 44); (70, 55); (64, 62); (54, 64); (38, 57); (38, 82); (30, 82); (30, 11); (38, 11); (38, 39); (40, 49); (45, 54); (52, 56); (60, 53); (63, 44); (63, 11); (72, 11); (72, 44)|]]
	|'i'->[[|(46,70);(54,70);(54,80);(46,80)|] ; [|(46,10);(54,10);(54,61);(46,61)|]]
	|'j'->[[|(60,61);(52,61);(52,6);(50,-3);(46,-4);(42,-4);(41,-11);(47,-12);(58,-7);(60,6);(60,61)|] ; [|(52,80);(52,70);(60,70);(60,80)|]]
	|'k'->[[|(72,9);(50,41);(70,61);(58,61);(37,40);(37,80);(29,80);(29,9);(37,9);(37,29);(44,35);(61,9);(72,9)|]]
	|'l'->[[|(46,9);(55,9);(55,80);(46,80)|]]
	|'m'->[[|(86, 54); (82, 67); (70, 71); (54, 62); (49, 69); (39, 71); (30, 69); (23, 63); (23, 70); (16, 70); (16, 18); (24, 18); (24, 45); (26, 56); (30, 62); (37, 64); (44, 61); (46, 52); (46, 18); (55, 18); (55, 49); (59, 60); (68, 64); (73, 62); (76, 59); (77, 51); (77, 18); (86, 18); (86, 54)|]]
	|'n'->[[|(71, 51); (70, 60); (67, 66); (62, 71); (53, 72); (36, 64); (36, 71); (29, 71); (29, 19); (37, 19); (37, 48); (41, 61); (51, 65); (57, 63); (61, 59); (62, 51); (62, 19); (71, 19); (71, 51)|]]
	|'o'->[[|(74, 47); (65, 47); (61, 32); (50, 27); (39, 32); (35, 47); (26, 47); (32, 27); (50, 20); (62, 23); (71, 32); (74, 47)|] ; [|(74, 48); (67, 67); (50, 74); (34, 69); (26, 47); (26, 47); (35, 47); (35, 47); (39, 62); (50, 67); (61, 62); (65, 48); (65, 47); (74, 47); (74, 48)|]]
	|'p'->[[|(72, 49); (63, 49); (59, 34); (49, 29); (40, 34); (36, 49); (36, 50); (28, 50); (28, 4); (36, 4); (36, 29); (42, 24); (50, 22); (61, 26); (70, 35); (72, 49)|] ; [|(73, 50); (70, 63); (62, 73); (51, 76); (42, 74); (36, 69); (36, 75); (28, 75); (28, 50); (36, 50); (40, 64); (50, 70); (60, 65); (64, 50); (63, 49); (72, 49); (73, 50)|]]
	|'q'->[[|(72, 53); (63, 53); (65, 49); (61, 34); (51, 29); (41, 34); (37, 50); (37, 52); (28, 52); (28, 50); (34, 30); (50, 22); (58, 24); (64, 29); (64, 4); (72, 4); (72, 53)|] ; [|(72, 75); (65, 75); (65, 68); (49, 76); (38, 73); (30, 64); (28, 52); (37, 52); (41, 65); (50, 70); (60, 64); (63, 53); (72, 53); (72, 75)|]]
	|'r'->[[|(64, 71); (55, 73); (49, 72); (43, 64); (43, 72); (36, 72); (36, 20); (44, 20); (44, 48); (46, 58); (49, 63); (54, 64); (61, 62); (64, 71)|]]	
	|'s'->[[|(72, 36); (70, 43); (64, 48); (51, 52); (43, 54); (40, 56); (39, 60); (41, 64); (50, 66); (58, 64); (61, 58); (70, 59); (67, 67); (60, 72); (49, 73); (42, 72); (36, 70); (32, 65); (30, 59); (32, 52); (38, 47); (51, 42); (60, 39); (63, 35); (60, 29); (51, 26); (41, 29); (37, 37); (29, 36); (35, 23); (51, 19); (62, 21); (69, 27); (72, 36)|]]
	|'t'->[[|(63, 18); (62, 25); (58, 25); (55, 26); (53, 27); (53, 32); (53, 63); (62, 63); (62, 69); (53, 69); (53, 87); (44, 82); (44, 69); (38, 69); (38, 63); (44, 63); (44, 33); (45, 22); (49, 18); (56, 17); (63, 18)|]]
	|'u'->[[|(71, 71); (62, 71); (62, 44); (60, 34); (55, 28); (48, 26); (41, 28); (38, 33); (37, 43); (37, 71); (29, 71); (29, 39); (29, 31); (32, 25); (38, 20); (46, 18); (63, 27); (63, 20); (71, 20); (71, 71)|]]
	|'v'->[[|(74, 71); (65, 71); (54, 40); (50, 30); (47, 40); (36, 71); (27, 71); (46, 20); (54, 20); (74, 71)|]]
	|'w'->[[|(86, 71); (86, 71); (77, 71); (68, 41); (65, 31); (63, 41); (55, 71); (46, 71); (38, 41); (35, 30); (32, 42); (24, 71); (15, 71); (30, 20); (40, 20); (50, 59); (52, 51); (60, 20); (69, 20); (86, 71)|]]
	|'x'->[[|(74, 21); (55, 48); (73, 72); (62, 72); (53, 60); (50, 54); (46, 60); (38, 72); (27, 72); (44, 48); (26, 21); (36, 21); (50, 41); (53, 37); (63, 21); (74, 21)|]]
	|'y'->[[|(74,80);(65,80);(54,50);(51,38);(47,50);(36,80);(27,80);(46,28);(45,26);(43,20);(40,17);(35,16);(30,17);(31,9);(37,8);(44,10);(49,16);(54,28);(74,80)|]]
	|'z'->[[|(73, 27); (73, 27); (49, 27); (38, 27); (44, 33); (72, 66); (72, 71); (29, 71); (29, 64); (51, 64); (61, 65); (28, 27); (28, 20); (73, 20); (73, 27)|]]
	|'A'->[[|(99,9);(69,80);(64,80);(64,73);(69,58);(76,38);(63,38);(63,31);(79,31);(88,9);(99,9)|] ; [|(66,80);(61,80);(34,9);(44,9);(51,31);(65,31);(65,38);(54,38);(62,59);(66,73);(66,73);(66,80)|]]
	|'B'->[[|(48,72);(61,72);(61,80);(39,80);(39,9);(61,9);(61,17);(48,17);(48,42);(61,42);(61,50);(48,50);(48,72)|] ; [|(88,30);(84,40);(79,44);(74,47);(82,53);(84,62);(81,72);(74,78);(60,80);(56,80);(56,72);(57,72);(68,71);(73,67);(75,61);(73,55);(68,51);(59,50);(56,50);(56,42);(60,42);(70,41);(76,37);(78,30);(76,24);(73,20);(67,18);(61,17);(56,17);(56,9);(61,9);(72,10);(80,14);(85,20);(88,30)|]]
	|'C'->[[|(82,30);(72,33);(65,19);(51,15);(39,18);(31,28);(28,44);(31,58);(38,68);(52,72);(64,69);(71,57);(81,60);(70,75);(52,80);(35,76);(23,63);(19,44);(22,25);(33,11);(52,6);(71,13);(82,30)|]]
	|'D'->[[|(79,41);(69,41);(68,31);(63,22);(56,19);(45,17);(30,17);(30,42);(21,42);(21,9);(46,9);(58,10);(66,14);(73,20);(78,31);(79,41)|] ; [|(80,45);(77,62);(68,74);(58,79);(45,80);(21,80);(21,42);(30,42);(30,72);(45,72);(57,71);(66,62);(70,45);(69,41);(79,41);(80,45)|]]
	|'E'->[[|(77,17);(33,17);(33,42);(73,42);(73,50);(33,50);(33,72);(75,72);(75,80);(24,80);(24,9);(77,9);(77,17)|]]
	|'F'->[[|(35,72);(74,72);(74,80);(26,80);(26,9);(35,9);(35,41);(69,41);(69,50);(35,50);(35,72)|]]
	|'G'->[[|(83,44);(52,44);(52,36);(73,36);(73,22);(65,17);(53,15);(39,18);(30,28);(26,44);(29,58);(34,65);(41,70);(52,72);(62,70);(69,65);(73,57);(82,59);(76,71);(66,78);(52,80);(33,76);(21,63);(17,43);(21,24);(34,11);(53,6);(68,9);(83,18);(83,44)|]]
	|'H'->[[|(78,80);(68,80);(68,51);(31,51);(31,80);(22,80);(22,9);(31,9);(31,43);(68,43);(68,9);(78,9);(78,80)|]]
	|'I'->[[|(46,9);(55,9);(55,80);(46,80)|]]
	|'J'->[[|(70,80);(60,80);(60,31);(59,22);(56,18);(50,16);(42,19);(39,30);(31,29);(35,13);(50,8);(61,10);(68,18);(70,32);(70,80)|]]
	|'K'->[[|(80,9);(48,51);(78,80);(66,80);(30,45);(30,80);(21,80);(21,9);(30,9);(30,34);(42,45);(67,9);(80,9)|]]
	|'L'->[[|(72,17);(37,17);(37,80);(28,80);(28,9);(72,9);(72,17)|]]
	|'M'->[[|(84,80);(71,80);(54,31);(50,19);(47,30);(30,80);(16,80);(16,9);(25,9);(25,70);(45,9);(54,9);(75,69);(75,9);(84,9);(84,80)|]]
	|'N'->[[|(78,80);(69,80);(69,24);(31,80);(22,80);(22,9);(31,9);(31,65);(68,9);(78,9);(78,80)|]]
	|'O'->[[|(84,43);(83,46);(74,47);(74,43);(67,22);(50,14);(32,22);(25,42);(27,48);(17,48);(16,42);(20,25);(32,11);(50,6);(67,11);(80,24);(84,43)|] ; [|(84,46);(81,63);(69,76);(51,80);(26,70);(18,48);(28,48);(34,65);(51,72);(64,69);(72,59);(75,47);(84,46)|]]
	|'P'->[[|(81,53);(71,53);(70,50);(57,46);(38,46);(38,54);(29,54);(29,9);(38,9);(38,38);(57,38);(78,44);(81,53)|] ; [|(82,60);(80,70);(75,76);(66,80);(55,80);(28,80);(28,54);(37,54);(37,72);(56,72);(64,71);(70,67);(73,59);(70,53);(80,53);(82,60)|]]
	|'Q'->[[|(85,9);(73,15);(81,28);(84,40);(73,41);(66,21);(52,27);(50,20);(59,16);(50,14);(32,22);(25,43);(26,45);(16,46);(16,43);(20,24);(32,11);(50,6);(67,11);(83,2);(85,9)|] ; [|(84,43);(80,63);(68,76);(50,80);(32,76);(20,63);(16,46);(26,45);(32,65);(50,72);(63,69);(71,59);(74,43);(73,41);(84,40);(84,43)|]]
	|'R'->[[|(82,9);(69,28);(61,38);(56,41);(71,48);(72,52);(61,52);(59,50);(48,49);(28,49);(28,52);(19,52);(19,9);(28,9);(28,41);(39,41);(44,40);(49,38);(53,34);(60,24);(70,9);(82,9)|] ; [|(79,61);(76,72);(68,79);(53,80);(22,80);(22,52);(31,52);(31,73);(54,73);(65,69);(69,61);(67,55);(64,52);(75,52);(79,61)|]]
	|'S'->[[|(72,43);(70,50);(64,55);(51,59);(43,61);(40,63);(39,67);(41,71);(50,73);(58,71);(61,65);(70,66);(67,74);(60,79);(49,80);(42,79);(36,77);(32,72);(30,66);(32,59);(38,54);(51,49);(60,46);(63,42);(60,36);(51,33);(41,36);(37,44);(29,43);(35,30);(51,26);(62,28);(69,34);(72,43)|]]
	|'T'->[[|(78,80);(22,80);(22,72);(45,72);(45,9);(55,9);(55,72);(78,72);(78,80)|]]
	|'U'->[[|(78,80);(68,80);(68,39);(64,21);(49,16);(39,19);(33,25);(31,39);(31,80);(22,80);(22,39);(24,21);(33,11);(50,8);(67,12);(75,22);(78,39);(78,80)|]]
	|'V'->[[|(83,80);(83,80);(73,80);(54,28);(50,17);(46,28);(28,80);(18,80);(45,9);(55,9);(83,80)|]]
	|'W'->[[|(87,80);(80,80);(71,44);(68,31);(63,51);(55,80);(46,80);(35,42);(32,31);(30,43);(21,80);(14,80);(29,23);(36,23);(49,67);(50,74);(52,67);(64,23);(71,23);(87,80)|]]
	|'X'->[[|(83,9);(56,47);(81,80);(71,80);(57,61);(51,53);(45,62);(32,80);(21,80);(45,46);(18,9);(29,9);(47,34);(50,39);(53,34);(71,9);(83,9)|]]
	|'Y'->[[|(83,80);(72,80);(58,59);(50,47);(43,59);(29,80);(18,80);(45,39);(45,9);(55,9);(55,39);(83,80)|]]
	|'Z'->[[|(78,17);(32,17);(37,22);(77,72);(77,80);(26,80);(26,72);(66,72);(58,64);(22,18);(22,9);(78,9);(78,17)|]]
	|'0'->[[|(74,38);(64,38);(61,20);(51,15);(41,20);(38,38);(29,38);(35,15);(51,7);(64,12);(72,24);(74,38)|] ; [|(73,44);(72,61);(67,71);(60,78);(50,80);(37,76);(29,64);(27,44);(28,38);(37,38);(36,44);(40,68);(50,73);(60,67);(64,44);(63,38);(73,38);(73,44)|]]
	|'1'->[[|(63,80);(57,80);(49,71);(37,62);(37,54);(46,59);(54,65);(54,9);(63,9);(63,80)|]]
	|'2'->[[|(74,17);(39,17);(42,22);(53,31);(66,43);(72,52);(74,61);(68,75);(51,80);(35,75);(28,60);(37,59);(41,69);(51,73);(61,70);(65,61);(61,50);(45,35);(33,24);(28,15);(27,9);(74,9);(74,17)|]]
	|'3'->[[|(73,30);(70,41);(60,47);(67,53);(70,62);(67,71);(60,78);(49,80);(35,76);(27,62);(36,60);(41,70);(49,73);(58,70);(61,62);(56,53);(46,50);(45,50);(44,42);(50,43);(60,39);(64,30);(60,19);(49,15);(40,18);(35,29);(27,27);(34,13);(49,7);(67,14);(73,30)|]]
	|'4'->[[|(75,34);(65,34);(65,39);(57,39);(57,34);(34,34);(38,39);(29,39);(26,34);(26,26);(57,26);(57,9);(65,9);(65,26);(75,26);(75,34)|] ; [|(65, 80); (58, 80); (29, 39); (38, 39); (57, 66); (57, 39); (65, 39);(65, 80)|]]
	|'5'->[[|(74,34);(74,34);(68,51);(52,57);(38,53);(42,72);(71,72);(71,80);(35,80);(28,44);(36,43);(42,48);(49,50);(60,45);(65,33);(60,21);(50,16);(41,19);(36,29);(27,29);(34,14);(50,9);(68,17);(74,34)|]]
	|'6'->[[|(74, 32); (68, 49); (53, 55); (53, 55); (52, 47); (61, 43); (65, 31);(61, 19); (52, 15); (52, 7); (63, 10); (71, 19); (74, 32)|] ; [|(73,63);(66,76);(52,80);(34,72);(27,42);(33,15);(51,7);(52,7);(52,15);(51,15);(44,17);(39,23);(37,32);(41,43);(51,47);(52,47);(53,55);(43,53);(35,45);(38,62);(44,71);(52,73);(60,69);(64,62);(73,63)|]]
	|'7'->[[|(73,80);(73,80);(27,80);(27,72);(62,72);(49,53);(40,30);(37,10);(46,10);(49,29);(59,54);(73,74);(73,80)|]]
	|'8'->[[|(77, 29); (73, 40); (63, 47); (71, 53); (74, 62); (68, 75); (53, 80); (52, 80); (53, 73); (53, 73); (61, 70); (65, 62); (62, 54); (53, 51); (53, 51); (53, 44); (53, 44); (64, 39); (68, 29); (64, 19); (53, 15); (53, 15); (53, 7); (70, 14); (77, 29)|] ; [|(54, 7); (54, 15); (47, 17); (41, 22); (40, 29); (44, 39); (54, 44); (54, 51); (46, 54); (42, 62); (46, 70); (54, 73); (53, 80); (39, 75); (33, 62); (36, 53); (44, 47); (34, 41); (31, 29); (37, 14); (54, 7); (54, 7)|]]
	|'9'->[[|(74,46);(73,51);(62,51);(60,45);(50,41);(40,45);(37,51);(28,51);(33,39);(48,33);(58,35);(65,42);(65,40);(64,30);(60,21);(55,16);(48,15);(40,17);(36,26);(28,25);(34,12);(48,7);(62,12);(71,24);(74,46)|] ; [|(62,51);(73,51);(71,66);(62,77);(49,80);(33,74);(27,56);(28,51);(37,51);(36,56);(40,68);(50,73);(60,69);(64,57);(62,51)|]]
	|'('->[[|(48, 48); (50, 66); (54, 80); (62, 94); (56, 94); (42, 70); (39, 48); (44, 22); (56, 1); (62, 1); (48, 48)|]]
	|')'->[[|(62, 48); (58, 70); (45, 94); (39, 94); (46, 80); (51, 66); (53, 48); (39, 1); (45, 1); (57, 22); (62, 48)|]]
	|'['->[[|(49, 87); (60, 87); (60, 94); (41, 94); (41, 3); (60, 3); (60, 10); (49, 10); (49, 87)|]]
	|']'->[[|(60, 94); (41, 94); (41, 87); (51, 87); (51, 10); (41, 10); (41, 3); (60, 3); (60, 94)|]]
	|'{'->[[|(50, 55); (53, 63); (54, 79); (56, 87); (62, 89); (64, 89); (64, 96); (61, 96); (55, 96); (51, 94); (48, 90); (46, 83); (46, 72); (45, 61); (42, 56); (36, 54); (36, 46); (42, 44); (45, 38); (46, 26); (46, 13); (49, 7); (54, 3); (61, 3); (64, 3); (64, 10); (62, 10); (56, 12); (54, 16); (54, 28); (51, 43); (44, 50); (50, 55)|]]
	|'}'->[[|(64, 54); (58, 56); (55, 61); (54, 73); (53, 86); (50, 92); (45, 96); (38, 96); (36, 96); (36, 89); (37, 89); (43, 87); (45, 83); (45, 72); (48, 57); (56, 50); (49, 44); (46, 36); (45, 20); (44, 12); (37, 10); (36, 10); (36, 3); (38, 3); (44, 3); (49, 5); (52, 9); (53, 16); (54, 27); (55, 38); (58, 44); (64, 46); (64, 54)|]]
	|'#'->[[|(68, 57); (78, 57); (78, 65); (69, 65); (74, 85); (66, 85); (62, 65); (54, 65); (53, 57); (61, 57); (57, 40); (49, 40); (48, 32); (55, 32); (51, 11); (59, 11); (63, 32); (78, 32); (78, 40); (64, 40); (68, 57)|] ; [|(46, 57); (54, 57); (55, 65); (48, 65); (52, 85); (45, 85); (40, 65); (26, 65); (26, 57); (39, 57); (35, 40); (26, 40); (26, 32); (34, 32); (30, 11); (37, 11); (41, 32); (49, 32); (50, 40); (43, 40); (46, 57)|]]
	|'*'->[[|(54,63);(66,66);(64,73);(52,68);(53,80);(46,80);(47,68);(36,73);(34,66);(46,63);(37,54);(43,50);(50,60);(56,50);(62,54);(54,63)|]]
	|'+'->[[|(74,61);(54,61);(54,80);(46,80);(46,61);(27,61);(27,53);(46,53);(46,33);(54,33);(54,53);(74,53);(74,61)|]]
	|'!'->[[|(45,9);(55,9);(55,19);(45,19)|] ; [|(55,80);(45,80);(45,65);(47,27);(53,27);(55,65);(55,80)|]]
	|'?'->[[|(45,8);(54,8);(54,18);(45,18)|] ; [|(73,61);(67,75);(50,80);(34,75);(27,60);(36,59);(41,70);(50,73);(60,69);(64,60);(62,55);(56,49);(50,43);(47,37);(45,28);(45,25);(54,25);(54,33);(56,37);(62,43);(71,52);(73,61)|]]
	|','->[[|(55, 30); (45, 30); (45, 20); (50, 20); (49, 14); (45, 10); (47, 6); (53, 12); (55, 20); (55, 30)|]]
	|'&'->[[|(84, 18); (72, 29); (79, 44); (70, 46); (66, 36); (53, 53); (67, 70); (62, 81); (51, 85); (51, 78); (56, 76); (58, 70); (56, 64); (51, 60); (51, 41); (61, 28); (54, 22); (51, 21); (51, 12); (57, 14); (67, 21); (78, 11); (84, 18)|] ; [|(54, 12); (54, 21); (49, 19); (39, 24); (36, 33); (39, 40); (48, 48); (54, 41); (54, 60); (52, 58); (48, 63); (45, 67); (45, 70); (47, 76); (53, 78); (54, 78); (54, 85); (53, 85); (41, 81); (36, 69); (37, 63); (44, 54); (31, 44); (27, 32); (32, 19); (49, 11); (54, 12)|]]
	|'/' -> [[|30,0 ; 40,0 ; 70,100 ; 60,100|]]
	|'|' -> [[|45,-10 ; 55,-10 ; 55,110 ; 45,110|]]
	|'\\' -> [[|30,100 ; 40,100 ; 70,0 ; 60,0|]]
	|':' -> [[|46,28 ; 54,28 ; 54,36 ; 46,36|];[|46,64 ; 54,64 ; 54,72 ; 46,72|]]
	|'=' -> [[|26,25 ; 74,25 ; 74,40 ; 26,40|];[|26,61 ; 74,61 ; 74,76 ; 26,76|]]
	|_ -> raise Undefined_character
;;

(*Dessine le caractere a la position et a la taille fourni en arguments*)
let dessiner_caractere c x y w h = 
	set_color black;
	let liste_poly = caractere_to_liste_poly c in
	dessiner_liste_poly (List.map (Array.map (calculer_coordonnees x y w h)) liste_poly) fill_poly;
;;

(********************************
Fonctions d'affichage
********************************)

(*Ouvre la fenetre graphique*)
let ouvrir_fenetre () = open_graph " 1280x720";;

(*Colorie le fond de la fenetre graphique*)
let afficher_background color = set_color color; fill_rect 0 0 1280 720;;

(*Affiche la case "action en cours"*)
let afficher_case_action () = 
	set_color white;
	fill_rect 320 130 640 170;
	set_color black;
	draw_rect 320 130 640 170;
	moveto 325 285;
	draw_string "Action en cours :"
;;

(*Affiche le ruban (vide) *)
let afficher_ruban () = 	
	let rec aux i =
	if i = 0 then ()
	else
		begin
		set_color white;
		fill_rect 51 336 ((i*59)-1) 99;
		set_color black;
		draw_rect 50 335 (i*59) 100;
		aux (i-1)
		end
	in aux 20
;;


(* Initialise la fenetre graphique *)
let initialiser_fenetre () = 
	begin 
		ouvrir_fenetre ();
		set_window_title "Simulateur de Machine de Turing"; 
		afficher_background (rgb 254 215 84); 
		afficher_case_action ();
		afficher_ruban ();
		(* Déplace le curseur courant *)
		moveto 507 0;
		set_color black;
		draw_string "Projet OCaml Guillevic Corentin et Blain Jarod";
		(*
		set_color white;
		fill_rect 50 335 1180 100;
		*)
		(* Tete de lecture *)
		set_color black;
		fill_poly[|670,450 ; 640,490 ; 700,490|];
		fill_rect 660 490 20 30;
		for i = 1 to 2 do
			draw_rect (640+i) (335+i) (59-2*i) (100-2*i)
		done;
		moveto 627 526;
		draw_string "Tete de lecture"	
	end
;;

(*Affiche str aux coordonnees x y*)
let afficher_string_element str x y =
	set_color black;
	moveto x y;
	draw_string str
;;

(*Affichage individuel de contenu de cases*)
let afficher_element str rang =
	if String.length str > 0 then 
		let x = 55+rang*59 in
		let y = 361 in
		let w = 49 in
		let h = 49 in
		try dessiner_caractere (String.get str 0) x y w h with
		|Undefined_character -> afficher_string_element str (75+rang*59) 380		
;;

(* 
Reinitialise et remplis le ruban 
Chacune des 20 cases fait 59px de large, 100 de haut
*)
let remplir_ruban liste_case =
	afficher_ruban ();
	let rec aux l indice = 
		match l with
		|[] -> ()
		|d::f -> afficher_element d indice;aux f (indice+1)
	in aux liste_case 0;; 

(*Affichage une transition*)
let afficher_transition transition = 
	moveto 325 221;
	set_color black;
	match transition with
	|Transition(a,b,c,d,e)->begin 
								draw_string (String.concat "" ["Etat courant : " ;(etat_to_string a);"   Symbole lu : '";(symbole_to_string b);"'  Le nouvel etat va etre : ";(etat_to_string c)]);
								moveto 325 201;
								draw_string (String.concat "" ["La tete de lecture va ecrire '";(symbole_to_string d);"' et se deplacer dans la direction ";(direction_to_string e)])
							end
;;
(*Affichage de l'action en cours*)
let afficher_action_en_cours modele = 
	afficher_case_action ();
	moveto 325 221;
	set_color black;
	if (est_dans_etat_final modele) then draw_string "La machine est dans un etat final."
	else
	try afficher_transition (trouver_transition (modele.mt.transitions) (modele.etat_courant) (lire_tete modele.tete_lecture)) with
	|Failure(c) -> draw_string c;raise Stop
;;

(*Actualise le ruban et l action en cours de l affichage*)
let afficher_mouvement_modele modele = remplir_ruban (get_ruban modele);afficher_action_en_cours modele;;

(********************************
Fonctions principales
********************************)

(*Applique un cycle a la machine de Turing si elle n est pas dans un etat final et affiche le resultat*)
let mouvement_modele_interface_graphique modele =
	if not (est_dans_etat_final modele) then let m = mouvement modele in afficher_mouvement_modele m;m
	else modele;;

(*Initialise l affichage pour l interface manuelle*)
let initialisation_affichage_interface_manuelle () = initialiser_fenetre ();dessiner_bouton bouton_step dessin_bouton_step;;

(*Initialise l affichage pour l interface automatique*)
let initialisation_affichage_interface_automatique () = initialiser_fenetre ();;

(*Boucle finale de l interface graphique : permet de fermer la fenetre sans causer d erreur*)
let interface_graphique_arret () = 
	try	
		while true do
			let event = Graphics.wait_next_event [Graphics.Button_down] in
			if event.Graphics.button then () 	
		done
	with|Graphics.Graphic_failure(_) -> close_graph ()
;;

(*Constitue la boucle infinie de l interface reagissant au clic de l utilisateur*)
let boucle_infinie_interface_manuelle modele=
	let rec aux m = 
		let event = Graphics.wait_next_event [Graphics.Button_down] in
		try 
			if event.Graphics.button && is_target bouton_step (event.Graphics.mouse_x) (event.Graphics.mouse_y) then begin aux (mouvement_modele_interface_graphique m) end
			else aux m
		with
		|Failure(s) -> print_string s; raise End
	in aux modele
;;

(*Constitue la boucle infinie de l interface appliquant des transitions de maniere automatique*)
let boucle_infinie_interface_automatique modele=
	let rec aux m = 
		try 
			Unix.sleep 1;
			aux (mouvement_modele_interface_graphique m)
		with
		|Failure(s) -> print_string s; raise End
	in aux modele
;;

(* Lance l interfaxce graphique en utilisant le "modele" initialiser avec "mot" en utilisant la "boucle" fournie*)
let lancer_interface_graphique initialisation boucle modele mot =
	initialisation();
	let m = changer_ruban_modele (reinitialiser_modele modele) mot in
	remplir_ruban (get_ruban m);
	afficher_action_en_cours m;
	try
		boucle m
	with
	|Stop -> interface_graphique_arret ()
	|End -> close_graph ()
	|Graphics.Graphic_failure(_) -> close_graph ();;	

(* Lance l interface graphique en utilisant le "modele" initialiser avec "mot" en mode manuel*)
let lancer_interface_graphique_manuelle modele mot = lancer_interface_graphique initialisation_affichage_interface_manuelle boucle_infinie_interface_manuelle modele mot;;

(* Lance l interface graphique en utilisant le "modele" initialiser avec "mot" en mode automatique*)
let lancer_interface_graphique_automatique modele mot = lancer_interface_graphique initialisation_affichage_interface_automatique boucle_infinie_interface_automatique modele mot;;
